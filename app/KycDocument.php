<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KycDocument extends Model
{
	protected $hidden = ['deleted_at', 'id', 'unique_id'];

	protected $appends = ['kyc_document_id', 'kyc_document_unique_id'];

    public function getKycDocumentIdAttribute() {

        return $this->id;
    }

    public function getKycDocumentUniqueIdAttribute() {

        return $this->unique_id;
    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCommonResponse($query) {

        return $query;
    
    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeApproved($query) {

        return $query->where('kyc_documents.status', APPROVED);
    
    }

    public static function boot() {

        parent::boot();

        static::creating(function ($model) {
            $model->attributes['unique_id'] = "KYC"."-".uniqid();
        });

        static::created(function($model) {

            $model->attributes['unique_id'] = "KYC"."-".$model->attributes['id']."-".uniqid();

            $model->save();
        
        });

    }
}
