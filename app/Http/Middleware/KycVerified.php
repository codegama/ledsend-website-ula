<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Http\Request;

use Validator, DB, Setting, Log, Helper;

use App\User;

class KycVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user_details = User::find($request->id);

        if(!$user_details) {
            
            $response = ['success' => false, 'error' => api_error(1002), 'error_code' => 1002];

            return response()->json($response, 200);

        }

        if($user_details->is_kyc_document_approved != USER_KYC_DOCUMENT_APPROVED) {
            
            // $response = ['success' => false, 'error' => api_error(1008), 'error_code' => 1008];

            // return response()->json($response, 200);
        
        }

        return $next($request);
    }
}
