<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB, Log, Hash, Validator, Exception, Setting, Helper;

use App\User;

class VerificationApiController extends Controller
{
    protected $loginUser, $skip, $take;

	public function __construct(Request $request) {

        Log::info(url()->current());

        Log::info("Request Data".print_r($request->all(), true));
        
        $this->loginUser = User::CommonResponse()->find($request->id);

        $this->skip = $request->skip ?: 0;

        $this->take = $request->take ?: (Setting::get('admin_take_count') ?: TAKE_COUNT);

        $this->timezone = $this->loginUser->timezone ?? "America/New_York";

    }

    /** 
     * @method kyc_documents_list()
     *
     * @uses To display the user details based on user  id
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request - User Id
     *
     * @return json response with user details
     */

    public function kyc_documents_list(Request $request) {

        try {

        	$kyc_documents = \App\KycDocument::CommonResponse()->get();

            $is_delete_edit_option = $this->loginUser->is_kyc_document_approved == USER_KYC_DOCUMENT_APPROVED ? NO : YES;


        	foreach ($kyc_documents as $key => $kyc_document) {

        		$is_user_uploaded = NO;

        		// Check the user uploaded the document

        		$user_kyc_document = \App\UserKycDocument::where('user_id', $request->id)->where('kyc_document_id', $kyc_document->kyc_document_id)->CommonResponse()->first();

        		$kyc_document->is_user_uploaded = $user_kyc_document ? YES : NO;

        		$kyc_document->user_kyc_document = $user_kyc_document ?? [];

                $kyc_document->is_delete_edit_option = $is_delete_edit_option;
        	}

        	$data['kyc_documents'] = $kyc_documents;

            return $this->sendResponse($message = "", $success_code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }

    /** 
     * @method kyc_documents_save()
     *
     * @uses To display the user details based on user  id
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request - User Id
     *
     * @return json response with user details
     */

    public function kyc_documents_save(Request $request) {

        try {

        	DB::beginTransaction();

             // Validation start

            $rules = [
            		'kyc_document_id' => 'required|exists:kyc_documents,id',
                    'document_file' => 'required',
                    'document_file_front' => '',
                    'document_file_back' => '',
            	];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end

            $request->request->add(['user_id' => $request->id]);

        	$kyc_document = \App\UserKycDocument::updateOrCreate(['kyc_document_id' => $request->kyc_document_id, 'user_id' => $request->id], $request->all());

        	$kyc_document->document_file = \Helper::storage_upload_file($request->file('document_file'), DOCUMENTS_PATH);

            $kyc_document->document_file_front = \Helper::storage_upload_file($request->file('document_file_front'), DOCUMENTS_PATH);
            
            $kyc_document->document_file_back = \Helper::storage_upload_file($request->file('document_file_back'), DOCUMENTS_PATH);

        	$kyc_document->save();

            if($user_details = User::find($request->id)) {

                if($user_details->is_kyc_document_approved != USER_KYC_DOCUMENT_APPROVED) {

                    $user_details->is_kyc_document_approved = USER_KYC_DOCUMENT_PENDING;

                    $user_details->save();
                }
            }

        	DB::commit();

            return $this->sendResponse(api_success(114), $success_code = 114, $kyc_document);

        } catch(Exception $e) {

        	DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }

    /** 
     * @method kyc_documents_delete()
     *
     * @uses To display the user details based on user  id
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request - User Id
     *
     * @return json response with user details
     */

    public function kyc_documents_delete(Request $request) {

        try {

            DB::beginTransaction();

             // Validation start

            $rules = ['user_kyc_document_id' => 'required|exists:user_kyc_documents,id,user_id,'.$request->id];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end

        	$user_billing_account = \App\UserKycDocument::destroy($request->user_kyc_document_id);

        	DB::commit();

        	$data['user_kyc_document_id'] = $request->user_kyc_document_id;

            return $this->sendResponse(api_success(115), $success_code = 115, $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }

    /** 
     * @method kyc_documents_delete_all()
     *
     * @uses delete user uploaded all documents
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request - User Id
     *
     * @return json response with user details
     */

    public function kyc_documents_delete_all(Request $request) {

        try {

            DB::beginTransaction();

        	\App\UserKycDocument::where('user_id', $request->id)->delete();

        	DB::commit();

            return $this->sendResponse(api_success(116), $success_code = 116, $data = []);

        } catch(Exception $e) {
        	
        	DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }

    /** 
     * @method users_kyc_status()
     *
     * @uses To display the user details based on user  id
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request - User Id
     *
     * @return json response with user details
     */

    public function kyc_status_user(Request $request) {

        try {

            $user_details = User::where('id' , $request->id)->CommonResponse()->first();

            if(!$user_details) { 

                throw new Exception(api_error(1002) , 1002);
            }

            return $this->sendResponse($message = "", $success_code = "", $user_details);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }

    /** 
     * @method users_accounts_list()
     *
     * @uses To display the user details based on user  id
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request - User Id
     *
     * @return json response with user details
     */

    public function users_accounts_list(Request $request) {

        try {

        	$user_billing_accounts = \App\UserBillingAccount::where('user_id', $request->id)->CommonResponse()->get();

        	$data['billing_accounts'] = $user_billing_accounts;

            return $this->sendResponse($message = "", $success_code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }

    /** 
     * @method users_accounts_view()
     *
     * @uses 
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request - User Id
     *
     * @return json response with user details
     */

    public function users_accounts_view(Request $request) {

        try {

            $user_billing_accounts = \App\UserBillingAccount::where('id', $request->user_billing_account_id)->CommonResponse()->get();

            $data['billing_accounts'] = $user_billing_accounts;

            return $this->sendResponse($message = "", $success_code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }

    /** 
     * @method users_accounts_save()
     *
     * @uses To display the user details based on user  id
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request - User Id
     *
     * @return json response with user details
     */

    public function users_accounts_save(Request $request) {

        try {

        	DB::beginTransaction();

             // Validation start

            $rules = [
            		'user_billing_account_id' => 'nullable|exists:user_billing_accounts,id',
                    'account_holder_name' => 'required',
                    'account_number' => 'required',
                    'ifsc_code' => 'required',
                    'swift_code' => 'required',
                    'nickname' => '',
            	];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end

            $request->request->add(['user_id' => $request->id]);

            if($request->user_billing_account_id) {
                
                $user_billing_account = \App\UserBillingAccount::updateOrCreate(['id' => $request->user_billing_account_id,'account_number' => $request->account_number, 'user_id' => $request->id], $request->all());

            } else {
                
                $user_billing_account = \App\UserBillingAccount::updateOrCreate(['account_number' => $request->account_number, 'user_id' => $request->id], $request->all());

            }

        	$user_billing_account->save();

        	DB::commit();

            return $this->sendResponse(api_success(112), $success_code = 112, $user_billing_account);

        } catch(Exception $e) {

        	DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }

    /** 
     * @method users_accounts_delete()
     *
     * @uses To display the user details based on user  id
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request - User Id
     *
     * @return json response with user details
     */

    public function users_accounts_delete(Request $request) {

        try {

            DB::beginTransaction();

             // Validation start

            $rules = ['user_billing_account_id' => 'required|exists:user_billing_accounts,id,user_id,'.$request->id];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end

        	$user_billing_account = \App\UserBillingAccount::destroy($request->user_billing_account_id);

        	DB::commit();

        	$data['user_billing_account_id'] = $request->user_billing_account_id;

            return $this->sendResponse(api_success(113), $success_code = 113, $data);

        } catch(Exception $e) {

        	DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }

    /** 
     * @method users_accounts_default()
     *
     * @uses To display the user details based on user  id
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request - User Id
     *
     * @return json response with user details
     */

    public function users_accounts_default(Request $request) {

        try {

            DB::beginTransaction();

             // Validation start

            $rules = ['user_billing_account_id' => 'required|exists:user_billing_accounts,id,user_id,'.$request->id];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end

            $old_accounts = \App\UserBillingAccount::where('user_id' , $request->id)->where('is_default', YES)->update(['is_default' => NO]);

            $user_billing_account = \App\UserBillingAccount::where('id' , $request->user_billing_account_id)->update(['is_default' => YES]);

            DB::commit();

            $data['user_billing_account'] = $user_billing_account;

            return $this->sendResponse(api_success(132), $success_code = 132, $data);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }
}
