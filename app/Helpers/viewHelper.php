<?php

use Carbon\Carbon;

use App\User, App\PageCounter, App\Settings;

use App\UserWalletPayment;

/**
 * @method tr()
 *
 * Description: used to convert the string to language based string
 *
 * @created Vidhya R
 *
 * @updated
 *
 * @param string $key
 *
 * @return string value
 */
function tr($key , $other_key = "" , $lang_path = "messages.") {

    // if(Auth::guard('admin')->check()) {

    //     $locale = config('app.locale');

    // } else {

        if (!\Session::has('locale')) {

            $locale = \Session::put('locale', config('app.locale'));

        }else {

            $locale = \Session::get('locale');

        }
    // }
    return \Lang::choice('messages.'.$key, 0, Array('other_key' => $other_key), $locale);

}

function api_success($key , $other_key = "" , $lang_path = "messages.") {

    if (!\Session::has('locale')) {

        $locale = \Session::put('locale', config('app.locale'));

    } else {

        $locale = \Session::get('locale');

    }
    return \Lang::choice('api-success.'.$key, 0, Array('other_key' => $other_key), $locale);

}

function api_error($key , $other_key = "" , $lang_path = "messages.") {

    if (!\Session::has('locale')) {

        $locale = \Session::put('locale', config('app.locale'));

    } else {

        $locale = \Session::get('locale');

    }
    return \Lang::choice('api-error.'.$key, 0, Array('other_key' => $other_key), $locale);

}

/**
 * @method envfile()
 *
 * Description: get the configuration value from .env file 
 *
 * @created Vidhya R
 *
 * @updated
 *
 * @param string $key
 *
 * @return string value
 */

function envfile($key) {

    $data = getEnvValues();

    if($data) {
        return $data[$key];
    }

    return "";

}

function getEnvValues() {

    $data =  [];

    $path = base_path('.env');

    if(file_exists($path)) {

        $values = file_get_contents($path);

        $values = explode("\n", $values);

        foreach ($values as $key => $value) {

            $var = explode('=',$value);

            if(count($var) == 2 ) {
                if($var[0] != "")
                    $data[$var[0]] = $var[1] ? $var[1] : null;
            } else if(count($var) > 2 ) {
                $keyvalue = "";
                foreach ($var as $i => $imp) {
                    if ($i != 0) {
                        $keyvalue = ($keyvalue) ? $keyvalue.'='.$imp : $imp;
                    }
                }
                $data[$var[0]] = $var[1] ? $keyvalue : null;
            }else {
                if($var[0] != "")
                    $data[$var[0]] = null;
            }
        }

        array_filter($data);
    
    }

    return $data;

}

/**
 * @method register_mobile()
 *
 * Description: Update the user register device details 
 *
 * @created Vidhya R
 *
 * @updated
 *
 * @param string $device_type
 *
 * @return - 
 */

function register_mobile($device_type) {

    // if($reg = MobileRegister::where('type' , $device_type)->first()) {

    //     $reg->count = $reg->count + 1;

    //     $reg->save();
    // }
    
}

/**
 * Function Name : subtract_count()
 *
 * Description: While Delete user, subtract the count from mobile register table based on the device type
 *
 * @created vithya R
 *
 * @updated vithya R
 *
 * @param string $device_ype : Device Type (Andriod,web or IOS)
 * 
 * @return boolean
 */

function subtract_count($device_type) {

    if($reg = MobileRegister::where('type' , $device_type)->first()) {

        $reg->count = $reg->count - 1;
        
        $reg->save();
    }

}

/**
 * @method get_register_count()
 *
 * Description: Get no of register counts based on the devices (web, android and iOS)
 *
 * @created Vidhya R
 *
 * @updated
 *
 * @param - 
 *
 * @return array value
 */

function get_register_count() {

    $ios_count = MobileRegister::where('type' , 'ios')->get()->count();

    $android_count = MobileRegister::where('type' , 'android')->get()->count();

    $web_count = MobileRegister::where('type' , 'web')->get()->count();

    $total = $ios_count + $android_count + $web_count;

    return array('total' => $total , 'ios' => $ios_count , 'android' => $android_count , 'web' => $web_count);

}

/**
 * @method: last_x_days_page_view()
 *
 * @uses: to get last x days page visitors analytics
 *
 * @created Anjana H
 *
 * @updated Anjana H
 *
 * @param - 
 *
 * @return array value
 */
function last_x_days_page_view($days){

    $views = PageCounter::orderBy('created_at','asc')->where('created_at', '>', Carbon::now()->subDays($days))->where('page','home');
 
    $arr = array();
 
    $arr['count'] = $views->count();

    $arr['get'] = $views->get();

      return $arr;
}

function counter($page = 'home'){

    $count_home = PageCounter::wherePage($page)->where('created_at', '>=', new DateTime('today'));

        if($count_home->count() > 0) {
            $update_count = $count_home->first();
            // $update_count->unique_id = uniqid();
            $update_count->count = $update_count->count + 1;
            $update_count->save();
        } else {
            $create_count = new PageCounter;
            $create_count->page = $page;
            // $create_count->unique_id = uniqid();
            $create_count->count = 1;
            $create_count->save();
        }

}

//this function convert string to UTC time zone

function convertTimeToUTCzone($str, $userTimezone, $format = 'Y-m-d H:i:s') {

    $new_str = new DateTime($str, new DateTimeZone($userTimezone));

    $new_str->setTimeZone(new DateTimeZone('UTC'));

    return $new_str->format( $format);
}

//this function converts string from UTC time zone to current user timezone

function convertTimeToUSERzone($str, $userTimezone, $format = 'Y-m-d H:i:s') {

    if(empty($str)){
        return '';
    }
    
    try {
        
        $new_str = new DateTime($str, new DateTimeZone('UTC') );
        
        $new_str->setTimeZone(new DateTimeZone( $userTimezone ));
    }
    catch(\Exception $e) {
        // Do Nothing
    }
    
    return $new_str->format( $format);
}

function number_format_short( $n, $precision = 1 ) {

    if ($n < 900) {
        // 0 - 900
        $n_format = number_format($n, $precision);
        $suffix = '';
    } else if ($n < 900000) {
        // 0.9k-850k
        $n_format = number_format($n / 1000, $precision);
        $suffix = 'K';
    } else if ($n < 900000000) {
        // 0.9m-850m
        $n_format = number_format($n / 1000000, $precision);
        $suffix = 'M';
    } else if ($n < 900000000000) {
        // 0.9b-850b
        $n_format = number_format($n / 1000000000, $precision);
        $suffix = 'B';
    } else {
        // 0.9t+
        $n_format = number_format($n / 1000000000000, $precision);
        $suffix = 'T';
    }
  // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
  // Intentionally does not affect partials, eg "1.50" -> "1.50"
    if ( $precision > 0 ) {
        $dotzero = '.' . str_repeat( '0', $precision );
        $n_format = str_replace( $dotzero, '', $n_format );
    }
    return $n_format . $suffix;

}

function common_date($date , $timezone , $format = "d M Y h:i A") {

    if($timezone) {

        $date = convertTimeToUSERzone($date , $timezone , $format);

    }

    return date($format , strtotime($date));
}

/**
 * function delete_value_prefix()
 * 
 * @uses used for concat string, while deleting the records from the table
 *
 * @created vidhya R
 *
 * @updated vidhya R
 *
 * @param $prefix - from settings table (Setting::get('prefix_user_delete'))
 *
 * @param $primary_id - Primary ID of the delete record
 *
 * @param $is_email 
 *
 * @return concat string based on the input values
 */

function delete_value_prefix($prefix , $primary_id , $is_email = 0) {

    if($is_email) {

        $site_name = str_replace(' ', '_', Setting::get('site_name'));

        return $prefix.$primary_id."@".$site_name.".com";
        
    } else {
        return $prefix.$primary_id;

    }

}

/**
 * function routefreestring()
 * 
 * @uses used for remove the route parameters from the string
 *
 * @created vidhya R
 *
 * @updated vidhya R
 *
 * @param string $string
 *
 * @return Route parameters free string
 */

function routefreestring($string) {

    $string = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $string));
    
    $search = [' ', '&', '%', "?",'=','{','}','$'];

    $replace = ['-', '-', '-' , '-', '-', '-' , '-','-'];

    $string = str_replace($search, $replace, $string);

    return $string;
    
}

/**
 * @method selected()
 *
 * @uses set selected item 
 *
 * @created Anjana H
 *
 * @updated Anjana H
 *
 * @param $array, $id, $check_key_name
 *
 * @return response of array 
 */
function selected($array, $id, $check_key_name) {
    
    $is_key_array = is_array($id);
    
    foreach ($array as $key => $array_details) {

        $array_details->is_selected = ($array_details->$check_key_name == $id) ? YES : NO;
    }  

    return $array;
}


function nFormatter($num, $currency = "") {

    $currency = Setting::get('currency', "$");

    if($num>1000) {

        $x = round($num);

        $x_number_format = number_format($x);

        $x_array = explode(',', $x_number_format);

        $x_parts = ['k', 'm', 'b', 't'];

        $x_count_parts = count($x_array) - 1;

        $x_display = $x;

        $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');

        $x_display .= $x_parts[$x_count_parts - 1];

        return $currency." ".$x_display;

    }

    return $currency." ".$num;

}

/**
 * @method formatted_plan()
 *
 * @uses used to format the number
 *
 * @created Bhawya
 *
 * @updated
 *
 * @param integer $num
 * 
 * @param string $currency
 *
 * @return string $formatted_plan
 */

function formatted_plan($plan = 0, $type = "month") {

    $text = $plan <= 1 ? tr('month') : tr('months');

    return $plan." ".$text;
}

function readFileLength($file)  {

    $variableLength = 0;
    if (($handle = fopen($file, "r")) !== FALSE) {
         $row = 1;
         while (($data = fgetcsv($handle, 1000, "\n")) !== FALSE) {
            $num = count($data);
            $row++;
            for ($c=0; $c < $num; $c++) {
                $exp = explode("=>", $data[$c]);
                if (count($exp) == 2) {
                    $variableLength += 1; 
                }
            }
        }
        fclose($handle);
    }

    return $variableLength;
}

function generate_payment_id() {

    $payment_id = time();

    $payment_id .= rand();

    $payment_id = sha1($payment_id);

    return strtoupper($payment_id);



}


/**
 * @method formatted_amount()
 *
 * @uses used to format the number
 *
 * @created vidhya R
 *
 * @updated vidhya R
 *
 * @param integer $num
 * 
 * @param string $currency
 *
 * @return string $formatted_amount
 */

function formatted_amount($amount = 0.00, $currency = "") {

    $currency = $currency ?: Setting::get('currency', '$');

    $amount = number_format((float)$amount, 2, '.', '');

    $formatted_amount = $currency."".$amount ?: "0.00";

    return $formatted_amount;
}

/**
 * @method get_wallet_message()
 *
 * @uses used to get the wallet message based on the types
 * 
 * @created vidhya R
 *
 * @updated vidhya R
 * 
 */

function get_wallet_message($user_wallet_payment) {

    $amount_type = $user_wallet_payment->payment_type;

    $status_text = [
        WALLET_PAYMENT_TYPE_ADD => tr('WALLET_PAYMENT_TYPE_ADD_TEXT'),WALLET_PAYMENT_TYPE_PAID => tr('WALLET_PAYMENT_TYPE_PAID_TEXT'), WALLET_PAYMENT_TYPE_CREDIT => tr('WALLET_PAYMENT_TYPE_CREDIT_TEXT'), WALLET_PAYMENT_TYPE_WITHDRAWAL => tr('WALLET_PAYMENT_TYPE_WITHDRAWAL_TEXT')];

    return isset($status_text[$amount_type]) ? $status_text[$amount_type] : tr('WALLET_PAYMENT_TYPE_ADD_TEXT');

}


function wallet_formatted_amount($amount = 0.00, $amount_type = WALLET_AMOUNT_TYPE_ADD) {

    $amount_symbol = $amount_type == WALLET_AMOUNT_TYPE_ADD ? "+" : "-";

    return $amount_symbol." ".formatted_amount($amount);

}

function last_days($days){

    $views = PageCounter::orderBy('created_at','asc')->where('created_at', '>', Carbon::now()->subDays($days))->where('page','home');
  
    $data = array();
  
    $data['count'] = $views->count();
  
    $data['get'] = $views->get();
  
    return $data;
  
  }


function wallet_details(){

    $start = new DateTime('first day of january', new DateTimeZone(Auth::guard('admin')->user()->timezone));
   
    $interval = DateInterval::createFromDateString('1 month');

    $period   = new DatePeriod($start, $interval, today());

    $wallet_details = array();
    
    foreach ($period as $key=>$dt) {
         
        $wallet_details[$key]['amount_add_to_wallet'] = UserWalletPayment::whereMonth('paid_date',$dt->format('m'))->whereYear('paid_date',$dt->format('Y'))->where('payment_type',WALLET_PAYMENT_TYPE_ADD)->sum('paid_amount');

        $wallet_details[$key]['sent_to_others'] = UserWalletPayment::whereMonth('paid_date',$dt->format('m'))->whereYear('paid_date',$dt->format('Y'))->where('payment_type',WALLET_PAYMENT_TYPE_PAID)->sum('paid_amount');
        
        $wallet_details[$key]['profit'] = ($wallet_details[$key]['amount_add_to_wallet'] - $wallet_details[$key]['sent_to_others']);

        $wallet_details[$key]['month'] = $dt->format('M');
    }

    return $wallet_details;
}

function paid_status_formatted($status) {

    $status_list = [
        USER_WALLET_PAYMENT_INITIALIZE => tr('USER_WALLET_PAYMENT_INITIALIZE'), 
        USER_WALLET_PAYMENT_PAID => tr('USER_WALLET_PAYMENT_PAID'), 
        USER_WALLET_PAYMENT_UNPAID => tr('USER_WALLET_PAYMENT_UNPAID'), 
        USER_WALLET_PAYMENT_CANCELLED => tr('USER_WALLET_PAYMENT_CANCELLED'),
        USER_WALLET_PAYMENT_DISPUTED => tr('USER_WALLET_PAYMENT_DISPUTED'),
        USER_WALLET_PAYMENT_WAITING => tr('USER_WALLET_PAYMENT_WAITING')
    ];

    return isset($status_list[$status]) ? $status_list[$status] : tr('paid');
}

function withdrawal_status_formatted($status) {

    $status_list = [WITHDRAW_INITIATED => tr('WITHDRAW_INITIATED'), WITHDRAW_PAID => tr('WITHDRAW_PAID'), WITHDRAW_ONHOLD => tr('WITHDRAW_ONHOLD'), WITHDRAW_DECLINED => tr('WITHDRAW_DECLINED'), WITHDRAW_CANCELLED => tr('WITHDRAW_CANCELLED')];

    return isset($status_list[$status]) ? $status_list[$status] : tr('WITHDRAW_INITIATED');
}

function dispute_status_formatted($status) {

    $status_list = [DISPUTE_SENT => tr('DISPUTE_SENT'), DISPUTE_APPROVED => tr('DISPUTE_APPROVED'), DISPUTE_DECLINED => tr('DISPUTE_DECLINED'), DISPUTE_CANCELLED => tr('DISPUTE_CANCELLED')];

    return isset($status_list[$status]) ? $status_list[$status] : tr('DISPUTE_SENT');
}

function message_type_formatted($message_type) {

    $message_type_list = [USER_TO_USER => tr('USER_TO_USER'), ADMIN_TO_USER => tr('ADMIN_TO_USER'), USER_TO_ADMIN => tr('USER_TO_ADMIN')];

    return isset($status_list[$message_type]) ? $message_type_list[$message_type] : tr('USER_TO_ADMIN');
}

function wallet_picture($amount_type = WALLET_AMOUNT_TYPE_ADD) {

    $wallet_picture = $amount_type == WALLET_AMOUNT_TYPE_ADD ? asset('images/wallet_plus.svg') : asset('images/wallet_minus.svg');

    return $wallet_picture;

}

function withdraw_picture($amount_type = WALLET_AMOUNT_TYPE_ADD) {

    $withdraw_picture = asset('images/withdraw_sent.svg');

    return $withdraw_picture;

}


function invoice_status_formatted($invoice_status) {

    $invoice_status_list = [INVOICE_DRAFT => tr('INVOICE_DRAFT'), INVOICE_SCHEDULED => tr('INVOICE_SCHEDULED'), INVOICE_SENT => tr('INVOICE_SENT'), INVOICE_PAID => tr('INVOICE_PAID')];

    return isset($invoice_status_list[$invoice_status]) ? $invoice_status_list[$invoice_status] : tr('INVOICE_DRAFT');
}

function kyc_status_formatted($status) {

    $status_list = [USER_KYC_DOCUMENT_NONE => tr('USER_KYC_DOCUMENT_NONE'), USER_KYC_DOCUMENT_PENDING => tr('USER_KYC_DOCUMENT_PENDING'), USER_KYC_DOCUMENT_APPROVED => tr('USER_KYC_DOCUMENT_APPROVED'), USER_KYC_DOCUMENT_DECLINED => tr('USER_KYC_DOCUMENT_DECLINED')];

    return isset($status_list[$status]) ? $status_list[$status] : tr('USER_KYC_DOCUMENT_NONE');
}


function formatted_amount_string($amount, $currency = "") {

    $currency = $currency ?: Setting::get('currency', '$');

    if($amount>1000) {

        $x = round($amount);
        $x_number_format = number_format($x);
        $x_array = explode(',', $x_number_format);
        $x_parts = array('K', 'M', 'B', 'T');
        $x_count_parts = count($x_array) - 1;
        $x_display = $x;
        $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
        $x_display .= $x_parts[$x_count_parts - 1];

        return $x_display;

    }

  return $currency.$amount;
}

function get_admin_picture() {
    
    return \App\Admin::first()->picture ?? asset('placeholder.jpg');
}