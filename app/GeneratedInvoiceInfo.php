<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GeneratedInvoiceInfo extends Model
{
    protected $hidden = ['deleted_at', 'id', 'unique_id'];

	protected $appends = ['generated_invoice_info_id'];

    public function getGeneratedInvoiceInfoIdAttribute() {

        return $this->id;
    } 

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCommonResponse($query) {
        return $query;
    }

    public static function boot() {

        parent::boot();

        static::creating(function ($model) {

        });

        static::created(function($model) {

            // $model->save();
        
        });
    }
}
