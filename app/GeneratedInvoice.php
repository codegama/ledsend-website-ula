<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GeneratedInvoice extends Model
{
    protected $hidden = ['deleted_at', 'id', 'unique_id'];

    protected $fillable = ['sender_id', 'invoice_number'];

	protected $appends = ['generated_invoice_id','generated_invoice_unique_id', 'sub_total_formatted', 'tax_total_formatted', 'total_formatted', 'status_formatted', 'to_username', 'to_user_picture','due_date_formatted','invoice_date_formatted'];

    public function getGeneratedInvoiceIdAttribute() {

        return $this->id;
    }

    public function getGeneratedInvoiceUniqueIdAttribute() {

        return $this->unique_id;
    }

    public function getTotalFormattedAttribute() {

        return formatted_amount($this->total);
    }

    public function getSubTotalFormattedAttribute() {

        return formatted_amount($this->sub_total);
    }

    public function getTaxTotalFormattedAttribute() {

        return formatted_amount($this->tax_total);
    }

    public function getStatusFormattedAttribute() {

        return invoice_status_formatted($this->status);
    }

    public function getInvoiceDateFormattedAttribute() {

        return common_date($this->invoice_date,'','d-m-Y');
    }
    public function getDueDateFormattedAttribute() {

        return common_date($this->due_date,'','d-m-Y');
    }

    public function getToUsernameAttribute() {

        return $this->to_user_id ? ($this->toUser->name ?? "-") : $this->to_user_email_address;
    }

    public function getToUserPictureAttribute() {

        return $this->to_user_id ? ($this->toUser->picture ?? "-") : asset('placeholder.jpg');
    }

    public function toUser() {
        return $this->belongsTo('App\User','to_user_id');
    }

    public function fromUser() {
        return $this->belongsTo('App\User','sender_id');
    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCommonResponse($query) {
        return $query;
    }

    public static function boot() {

        parent::boot();

        static::creating(function ($model) {

            $model->attributes['unique_id'] = "GIN-".uniqid();
        });

        static::created(function($model) {

            $model->attributes['unique_id'] = "GIN-".$model->attributes['id']."-".uniqid();

            $model->save();
        
        });
    }
}
