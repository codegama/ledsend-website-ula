<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCard extends Model
{
	protected $hidden = ['deleted_at', 'id', 'unique_id'];

	protected $appends = ['user_card_id'];

    public function getUserCardIdAttribute() {

        return $this->id;
    }
}
