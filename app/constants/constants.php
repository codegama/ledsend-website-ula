<?php

/*
|--------------------------------------------------------------------------
| Application Constants
|--------------------------------------------------------------------------
|
| 
|
*/

if(!defined('TAKE_COUNT')) define('TAKE_COUNT', 6);

if(!defined('NO')) define('NO', 0);
if(!defined('YES')) define('YES', 1);

if(!defined('PAID')) define('PAID',1);
if(!defined('UNPAID')) define('UNPAID', 0);
if (!defined('PAID_STATUS')) define('PAID_STATUS', 1);

if(!defined('DEVICE_ANDROID')) define('DEVICE_ANDROID', 'android');
if(!defined('DEVICE_IOS')) define('DEVICE_IOS', 'ios');
if(!defined('DEVICE_WEB')) define('DEVICE_WEB', 'web');

if(!defined('APPROVED')) define('APPROVED', 1);
if(!defined('DECLINED')) define('DECLINED', 0);

if(!defined('DEFAULT_TRUE')) define('DEFAULT_TRUE', true);
if(!defined('DEFAULT_FALSE')) define('DEFAULT_FALSE', false);

if(!defined('ADMIN')) define('ADMIN', 'admin');
if(!defined('USER')) define('USER', 'user');
if(!defined('PROVIDER')) define('PROVIDER', 'provider');

if(!defined('COD')) define('COD',   'COD');
if(!defined('PAYPAL')) define('PAYPAL', 'PAYPAL');
if(!defined('CARD')) define('CARD',  'CARD');
if(!defined('BANK_TRANSFER')) define('BANK_TRANSFER',  'BANK_TRANSFER');
if(!defined('PAYMENT_OFFLINE')) define('PAYMENT_OFFLINE',  'OFFLINE');
if(!defined('PAYMENT_MODE_WALLET')) define('PAYMENT_MODE_WALLET',  'WALLET');
if(!defined('RAZORPAY')) define('RAZORPAY', 'RAZORPAY');

//////// USERS

if(!defined('USER_PENDING')) define('USER_PENDING', 0);
if(!defined('USER_APPROVED')) define('USER_APPROVED', 1);
if(!defined('USER_DECLINED')) define('USER_DECLINED', 2);

if(!defined('USER_EMAIL_NOT_VERIFIED')) define('USER_EMAIL_NOT_VERIFIED', 0);
if(!defined('USER_EMAIL_VERIFIED')) define('USER_EMAIL_VERIFIED', 1);


//////// USERS END

/***** ADMIN CONTROLS KEYS ********/

if(!defined('ADMIN_CONTROL_ENABLED')) define('ADMIN_CONTROL_ENABLED', 1);
if(!defined('ADMIN_CONTROL_DISABLED')) define('ADMIN_CONTROL_DISABLED', 0);

if(!defined('USER_KYC_DOCUMENT_NONE')) define('USER_KYC_DOCUMENT_NONE', 0);
if(!defined('USER_KYC_DOCUMENT_PENDING')) define('USER_KYC_DOCUMENT_PENDING', 1);
if(!defined('USER_KYC_DOCUMENT_APPROVED')) define('USER_KYC_DOCUMENT_APPROVED', 2);
if(!defined('USER_KYC_DOCUMENT_DECLINED')) define('USER_KYC_DOCUMENT_DECLINED', 3);



if(!defined('USER_WALLET_PAYMENT_INITIALIZE')) define('USER_WALLET_PAYMENT_INITIALIZE', 0);
if(!defined('USER_WALLET_PAYMENT_PAID')) define('USER_WALLET_PAYMENT_PAID', 1);
if(!defined('USER_WALLET_PAYMENT_UNPAID')) define('USER_WALLET_PAYMENT_UNPAID', 2);
if(!defined('USER_WALLET_PAYMENT_CANCELLED')) define('USER_WALLET_PAYMENT_CANCELLED', 3);
if(!defined('USER_WALLET_PAYMENT_DISPUTED')) define('USER_WALLET_PAYMENT_DISPUTED', 4);
if(!defined('USER_WALLET_PAYMENT_WAITING')) define('USER_WALLET_PAYMENT_WAITING', 5);


// amount_type - add and debitedd
if(!defined('WALLET_AMOUNT_TYPE_ADD')) define('WALLET_AMOUNT_TYPE_ADD', 'add');
if(!defined('WALLET_AMOUNT_TYPE_MINUS')) define('WALLET_AMOUNT_TYPE_MINUS', 'minus');

// payment type - specifies the transaction usage
if(!defined('WALLET_PAYMENT_TYPE_ADD')) define('WALLET_PAYMENT_TYPE_ADD', 'add');
if(!defined('WALLET_PAYMENT_TYPE_PAID')) define('WALLET_PAYMENT_TYPE_PAID', 'paid');
if(!defined('WALLET_PAYMENT_TYPE_CREDIT')) define('WALLET_PAYMENT_TYPE_CREDIT', 'credit');
if(!defined('WALLET_PAYMENT_TYPE_WITHDRAWAL')) define('WALLET_PAYMENT_TYPE_WITHDRAWAL', 'withdrawal');

// withdrawal status
if(!defined('WITHDRAW_INITIATED')) define('WITHDRAW_INITIATED', 0);

if(!defined('WITHDRAW_PAID')) define('WITHDRAW_PAID', 1);

if(!defined('WITHDRAW_ONHOLD')) define('WITHDRAW_ONHOLD', 2);

if(!defined('WITHDRAW_DECLINED')) define('WITHDRAW_DECLINED', 3);

if(!defined('WITHDRAW_CANCELLED')) define('WITHDRAW_CANCELLED', 4);


if(!defined('INVOICE_DRAFT')) define('INVOICE_DRAFT', 0);

if(!defined('INVOICE_SCHEDULED')) define('INVOICE_SCHEDULED', 1);

if(!defined('INVOICE_SENT')) define('INVOICE_SENT', 2);

if(!defined('INVOICE_PAID')) define('INVOICE_PAID', 3);


if(!defined('MALE')) define('MALE',   'male');

if(!defined('FEMALE')) define('FEMALE',   'female');


if(!defined('DISPUTE_SENDER_TO_RECEIVER')) define('DISPUTE_SENDER_TO_RECEIVER', 'DSR');

if(!defined('DISPUTE_RECEIVER_TO_SENDER')) define('DISPUTE_RECEIVER_TO_SENDER', 'DRS');

if(!defined('DISPUTE_ADMIN_TO_USER')) define('DISPUTE_ADMIN_TO_USER', 'DAU');

if(!defined('USER_TO_USER')) define('USER_TO_USER', 'UU');

if(!defined('ADMIN_TO_USER')) define('ADMIN_TO_USER','AU');

if(!defined('USER_TO_ADMIN')) define('USER_TO_ADMIN','UA');


if(!defined('LOGIN_BY_MANUAL')) define('LOGIN_BY_MANUAL', 'manual');


if(!defined('DISPUTE_SENT')) define('DISPUTE_SENT', 1);

if(!defined('DISPUTE_APPROVED')) define('DISPUTE_APPROVED', 2);

if(!defined('DISPUTE_DECLINED')) define('DISPUTE_DECLINED', 3);

if(!defined('DISPUTE_CANCELLED')) define('DISPUTE_CANCELLED', 4);


if(!defined('STRIPE_MODE_LIVE')) define('STRIPE_MODE_LIVE', 'live');

if(!defined('STRIPE_MODE_SANDBOX')) define('STRIPE_MODE_SANDBOX', 'sandbox');

if(!defined('PUSH_TO_ALL')) define('PUSH_TO_ALL', 'all');

if(!defined('PUSH_TO_ANDROID')) define('PUSH_TO_ANDROID', 'android');

if(!defined('PUSH_TO_IOS')) define('PUSH_TO_IOS', 'ios');

if(!defined('OFF')) define('OFF', 0);

if(!defined('ON')) define('ON', 1);