<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCoreRelatedTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if(!Schema::hasTable('user_wallets')) {

            Schema::create('user_wallets', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('unique_id')->default(rand());
                $table->integer('user_id');
                $table->float('total')->default(0.00);
                $table->float('onhold')->default(0.00);
                $table->float('used')->default(0.00);
                $table->float('remaining')->default(0.00);
                $table->tinyInteger('status')->default(1);
                $table->timestamps();
            });
            
        }

        if(!Schema::hasTable('user_wallet_payments')) {

            Schema::create('user_wallet_payments', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('unique_id')->default(rand());
                $table->integer('user_id');
                $table->integer('to_user_id')->default(0);
                $table->integer('received_from_user_id')->default(0);
                $table->integer('generated_invoice_id')->default(0);
                $table->string('payment_id');
                $table->string('payment_type')->default(WALLET_PAYMENT_TYPE_ADD)->comment("add, paid, credit");
                $table->string('amount_type')->default(WALLET_PAYMENT_TYPE_ADD)->comment("add, minus");
                $table->float('requested_amount')->default(0.00);
                $table->float('paid_amount')->default(0.00);
                $table->string('currency')->default('$');
                $table->string('payment_mode')->default(CARD);
                $table->dateTime('paid_date')->nullable();
                $table->string('message')->default("");
                $table->integer('is_cancelled')->default(0);
                $table->string('cancelled_reason')->default("");
                $table->string('updated_by')->default('user')->comment('admin, user');
                $table->string('bank_statement_picture')->default('');
                $table->tinyInteger('is_admin_approved')->default(0);
                $table->integer('user_billing_account_id')->default(0);
                $table->tinyInteger('status')->default(0);
                $table->timestamps();
            });
            
        }

        if(!Schema::hasTable('user_withdrawals')) {

            Schema::create('user_withdrawals', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('unique_id')->default(rand());
                $table->integer('user_id');
                $table->integer('user_wallet_payment_id')->default(0);
                $table->string('payment_id')->default("");
                $table->string('payment_mode')->default(PAYMENT_OFFLINE);
                $table->float('requested_amount')->default(0.00);
                $table->float('paid_amount')->default(0.00);
                $table->text('cancel_reason')->nullable();
                $table->integer('user_billing_account_id')->default(0);
                $table->tinyInteger('status')->default(0)->comment("0 - pending, 1 - paid, 2 - rejected");
                $table->timestamps();
            });
            
        }

        // Not using now

        if(!Schema::hasTable('user_loans')) {

            Schema::create('user_loans', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('unique_id')->default(rand());
                $table->integer('user_id');
                $table->string('currency')->default("$");
                $table->float('loan_amount')->default(0.00);
                $table->float('loan_paid_amount')->default(0.00);
                $table->float('loan_remaining_amount')->default(0.00);
                $table->text('message')->nullable();
                $table->tinyInteger('status')->default(1);
                $table->timestamps();
            });
            
        }
        
        // Not using now

        if(!Schema::hasTable('user_loan_payments')) {

            Schema::create('user_loan_payments', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('unique_id')->default(rand());
                $table->integer('user_id');
                $table->string('payment_id')->nullable();
                $table->float('requested_paid_amount')->default(0.00);
                $table->float('paid_amount')->default(0.00);
                $table->text('message')->nullable();
                $table->tinyInteger('status')->default(1);
                $table->timestamps();
            });
            
        }

        // Not using now

        if(!Schema::hasTable('inboxes')) {

            Schema::create('inboxes', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('unique_id')->default(rand());
                $table->integer('user_dispute_id');
                $table->integer('sender_id');
                $table->integer('receiver_id');
                $table->string('message_type')->comment('AU - Admin to User', 'UU - User to User');
                $table->string('subject');
                $table->string('message');
                $table->tinyInteger('status')->default(1);
                $table->timestamps();
            });
            
        }

        if(!Schema::hasTable('resolution_centers')) {

            Schema::create('resolution_centers', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('unique_id')->default(rand());
                $table->integer('user_dispute_id');
                $table->integer('user_wallet_payment_id');                
                $table->integer('sender_id');
                $table->integer('receiver_id');
                $table->text('message');
                $table->string('subject');
                $table->string('message_type')->comment('AU - Admin to User', 'UA - User to Admin');
                $table->tinyInteger('status')->default(1);
                $table->timestamps();            
            });
        }

        if(!Schema::hasTable('generated_invoices')) {

            Schema::create('generated_invoices', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('unique_id')->unique();
                $table->integer('sender_id');
                $table->integer('to_user_id')->default(0);
                $table->string('to_user_email_address')->default("");
                $table->string('cc_user_id')->default(0);
                $table->string('cc_user_email_address')->default("");
                $table->string('title')->default("");
                $table->string('logo')->default(asset('invoice_business_logo.jpg'));
                $table->string('invoice_number');
                $table->dateTime('invoice_date')->nullable();
                $table->string('reference')->default('');
                $table->dateTime('due_date')->nullable();
                $table->text('notes')->nullable();
                $table->text('terms_conditions')->nullable();
                $table->float('sub_total')->default(0.00);
                $table->float('tax_total')->default(0.00);
                $table->float('total')->default(0.00);
                $table->tinyInteger('paid_status')->default(0)->comment('0 - not assigned, 1 - waiting for payment, 2 - paid, 3 - expired, 4 - cancelled');
                $table->tinyInteger('status')->default(0)->comment('0 - draft, 1 - sent');
                $table->timestamps();
            });
            
        }

        if(!Schema::hasTable('generated_invoice_infos')) {

            Schema::create('generated_invoice_infos', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('generated_invoice_id');
                $table->string('business_name')->default('');
                $table->string('first_name')->default('');
                $table->string('last_name')->default('');
                $table->text('address')->nullable();
                $table->string('mobile')->default('');
                $table->string('email')->default('');
                $table->string('website')->default('');
                $table->string('tax_number')->default('');
                $table->text('additional_information')->nullable();
                $table->tinyInteger('status')->default(1);
                $table->timestamps();
            });
        
        }

        if(!Schema::hasTable('generated_invoice_items')) {

            Schema::create('generated_invoice_items', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('unique_id')->unique(rand());
                $table->integer('generated_invoice_id');
                $table->string('name');
                $table->float('amount')->default(0.00);
                $table->float('quantity')->default(1);
                $table->float('sub_total')->default(0);
                $table->float('tax_price')->default(0);
                $table->float('total')->default(0.00);
                $table->tinyInteger('status')->default(1);
                $table->timestamps();
            });
        
        }

        if(!Schema::hasTable('user_disputes')) {

            Schema::create('user_disputes', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('unique_id')->unique();
                $table->integer('user_id');
                $table->integer('receiver_user_id')->default(0)->comment("who responsibl for the dispute");
                $table->integer('user_wallet_payment_id');
                $table->text('message')->nullable();
                $table->float('amount')->default(0.00);
                $table->text('cancel_reason')->nullable();
                $table->tinyInteger('status')->default(1);
                $table->timestamps();
            });
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_payments');
        Schema::dropIfExists('user_wallets');
        Schema::dropIfExists('user_wallet_payments');
        Schema::dropIfExists('user_redeem_requests');
        Schema::dropIfExists('user_loans');
        Schema::dropIfExists('user_loan_payments');
        Schema::dropIfExists('inboxes');
        Schema::dropIfExists('resolution_centers');
        Schema::dropIfExists('generated_invoices');
        Schema::dropIfExists('user_disputes');
    }
}
