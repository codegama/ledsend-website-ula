<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserWalletPaymentIdToGeneratedInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('generated_invoices', function (Blueprint $table) {
            $table->integer('user_wallet_payment_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('generated_invoices', function (Blueprint $table) {
            $table->dropColumn('user_wallet_payment_id');
        });
    }
}
