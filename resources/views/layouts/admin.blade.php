<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>

    <meta charset="utf-8" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>{{Setting::get('site_name')}}</title>

    @include('layouts.admin.styles')


    @yield('styles')
</head>

<body class="navbar-fixed sidebar-fixed" id="body">

@include('layouts.admin.scripts')

    <!-- <div id="toaster"></div> -->

    <div class="wrapper">

        @include('layouts.admin.sidebar')

        <div class="page-wrapper">

            @include('layouts.admin.header')

            <div class="content-wrapper">

                <div class="content">

                    <nav aria-label="breadcrumb">

                        <ol class="breadcrumb">

                            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">{{tr('home')}}</a></li>
                            
                            @yield('breadcrumbs')
                            
                        </ol>
                    </nav>

                    <div class="row">

                        <div class="col-lg-12">
                           
                            @include('notifications.notify')
                            
                            @yield('content')
                            
                        </div>

                    </div>

                </div>

            </div>
            
            @include('layouts.admin.footer')

        </div>

    </div>

    @include('layouts.admin._logout_model')

    @yield('scripts')

</body>

</html>