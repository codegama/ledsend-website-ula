<aside class="left-sidebar sidebar-dark" id="left-sidebar">

    <div id="sidebar" class="sidebar sidebar-with-footer">
        <!-- Aplication Brand -->
        <div class="app-brand">
            <a href="{{route('admin.dashboard')}}">
                <!-- <img src="{{Setting::get('site_logo')}}" alt="{{Setting::get('site_name')}}" class="logo"> -->
                <span class="brand-name text-uppercase">{{Setting::get('site_name')}}</span>
            </a>
        </div>
        <!-- begin sidebar scrollbar -->
        <div class="sidebar-left" data-simplebar style="height: 100%;">
            <!-- sidebar menu -->
            <ul class="nav sidebar-inner" id="sidebar-menu">

                <li class="has-sub" id="dashboard">
                    <a class="sidenav-item-link" href="{{route('admin.dashboard')}}">
                        <i class="mdi mdi-briefcase-account-outline"></i>
                        <span class="nav-text">{{tr('dashboard')}}</span>
                    </a>
                </li>

                <!-- account_management start -->

                <li class="section-title">{{tr('account_management')}}</li>

                <li class="has-sub" id="users_crud">

                    <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#users" aria-expanded="false" aria-controls="users">
                        <i class="mdi mdi-account-multiple"></i>
                        <span class="nav-text">{{tr('users')}}</span> <b class="caret"></b>
                    </a>
                    <ul class="collapse" id="users" data-parent="#sidebar-menu">
                        <div class="sub-menu">

                            <li id="users-create">
                                <a class="sidenav-item-link" href="{{route('admin.users.create')}}">
                                    <span class="nav-text">{{tr('add_user')}}</span>

                                </a>
                            </li>

                            <li id="users-index">
                                <a class="sidenav-item-link" href="{{route('admin.users.index')}}">
                                    <span class="nav-text">{{tr('view_users')}}</span>

                                </a>
                            </li>

                        </div>
                    </ul>

                </li>

                <li id="user_unverified">
                    <a class="sidenav-item-link" href="{{route('admin.unverified_users.index')}}">
                        <i class="mdi mdi-arrange-send-to-back"></i>
                        <span class="nav-text">Unverified Users</span>
                    </a>
                </li>

                <!-- account_management end -->

                <li class="section-title"><span class="title">{{tr('wallet_management')}}</span></li>

                <li class="has-sub" id="wallets_management">
                    <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#user_wallets" aria-expanded="true" aria-controls="user_wallets">
                        <i class="mdi mdi-wallet"></i>
                        <span class="nav-text">{{tr('wallets')}}</span> <b class="caret"></b>
                    </a>
                    <ul class="collapse" id="user_wallets" data-parent="#sidebar-menu">
                        <div class="sub-menu">

                            <li id="user_wallets-index">
                                <a class="sidenav-item-link" href="{{route('admin.user_wallets.index')}}">
                                    <i class="fa fa-circle-o"></i>{{tr('user_wallets')}}
                                </a>
                            </li>

                            <li id="user_wallets-transactions" class="">
                                <a class="sidenav-item-link" href="{{route('admin.user_wallet_payments.index')}}">
                                    <i class="fa fa-circle-o"></i>{{tr('transactions')}}
                                </a>
                            </li>

                            <li id="bank_transactions" class="">
                                <a class="sidenav-item-link" href="{{route('admin.user_wallet_payments.index',['type'=>BANK_TRANSFER])}}">
                                    <i class="fa fa-circle-o"></i>{{tr('bank_transactions')}}
                                </a>
                            </li>

                        </div>
                    </ul>

                </li>

                <li class="has-sub" id="invoices_management">

                    <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#invoices" aria-expanded="true" aria-controls="invoices">
                        <i class="mdi mdi-cash-multiple"></i>
                        <span class="nav-text">{{tr('invoices')}}</span> <b class="caret"></b>
                    </a>
                    <ul class="collapse" id="invoices" data-parent="#sidebar-menu">
                        <div class="sub-menu">

                            <!-- <li id="invoices-user">
                                <a href="#">
                                    <i class="fa fa-circle-o"></i>Create Invoice
                                </a>
                            </li> -->

                            <li id="invoices-view">
                                <a class="sidenav-item-link" href="{{route('admin.generated_invoices.index')}}">
                                    <i class="fa fa-circle-o"></i>{{tr('view_invoices')}}
                                </a>
                            </li>

                            <li id="invoices-pending">
                                <a class="sidenav-item-link" href="{{route('admin.generated_invoices.index',['pending_invoices'=>YES])}}">
                                    <i class="fa fa-circle-o"></i>{{tr('pending_invoices')}}
                                </a>
                            </li>

                        </div>
                    </ul>

                </li>

                <li class="has-sub" id="resolution_center_management" style="display: none;">

                    <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#resolution_center" aria-expanded="true" aria-controls="resolution_center">
                        <i class="mdi mdi-flag"></i>
                        <span class="nav-text">{{tr('resolution_center')}}</span> <b class="caret"></b>
                    </a>
                    <ul class="collapse" id="resolution_center" data-parent="#sidebar-menu">
                        <div class="sub-menu">

                            <li id="resolution-center-all">
                                <a class="sidenav-item-link" href="{{route('admin.resolution_center_index')}}">
                                    <i class="fa fa-circle-o"></i> {{tr('all_cases')}}
                                </a>
                            </li>

                            <li id="resolution-center-opened">
                                <a class="sidenav-item-link" href="{{route('admin.resolution_center_index',['type'=>NO])}}">
                                    <i class="fa fa-circle-o"></i>{{tr('opened_cases')}}
                                </a>
                            </li>

                            <li id="resolution-center-closed">
                                <a class="sidenav-item-link" href="{{route('admin.resolution_center_index',['type'=>YES])}}">
                                    <i class="fa fa-circle-o"></i>{{tr('closed_cases')}}
                                </a>
                            </li>

                        </div>
                    </ul>

                </li>

                <!-- <li id="resolution-center">
                    <a class="sidenav-item-link" href="{{route('admin.resolution_center_index')}}">
                        <i class="mdi mdi-flag"></i>
                        <span class="nav-text">{{tr('resolution_center')}}</span>
                    </a>
                </li> -->

                <li id="withdrawals_list">
                    <a class="sidenav-item-link" href="{{route('admin.user_withdrawals.index')}}">
                        <!-- <i class="mdi mdi-cannabis"></i> -->
                        <img src="{{asset('images/atm.svg')}}" width="25" height="25" />
                        <span class="nav-text break_word_css">{{tr('withdraw_requests')}}</span>
                    </a>
                </li>

                <li id="user_disputes">
                    <a class="sidenav-item-link" href="{{route('admin.user_disputes.index')}}">
                        <i class="mdi mdi-bullhorn"></i>
                        <span class="nav-text">{{tr('disputes')}}</span>
                    </a>
                </li>

                <li class="section-title">
                    <span class="title">Lookup Management</span>
                </li>

                <li class="has-sub" id="lookup_management">

                    <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#lookup_management_documents" aria-expanded="false" aria-controls="users">
                        <i class="mdi mdi-file-hidden"></i>
                        <span class="nav-text">Documents</span> <b class="caret"></b>
                    </a>

                    <ul class="collapse" id="lookup_management_documents" data-parent="#sidebar-menu">
                        <div class="sub-menu">

                            <li id="kyc_documents_create">
                                <a class="sidenav-item-link" href="{{route('admin.kyc_documents.create')}}">
                                    <span class="nav-text">Add Documents</span>

                                </a>
                            </li>

                            <li id="kyc_documents_index">
                                <a class="sidenav-item-link" href="{{route('admin.kyc_documents.index')}}">
                                    <span class="nav-text">View Documents</span>

                                </a>
                            </li>

                            <!-- <li id="users-index">
                                <a class="sidenav-item-link" href="{{route('admin.kyc_documents.index')}}">
                                    <span class="nav-text">User Documents</span>

                                </a>
                            </li> -->

                        </div>
                    </ul>

                </li>

                <li class="has-sub" id="static_pages_crud">
                    <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#static_pages" aria-expanded="true" aria-controls="static_pages">
                        <i class="mdi mdi-file-multiple"></i>
                        <span class="nav-text">{{tr('static_pages')}}</span> <b class="caret"></b>
                    </a>
                    <ul class="collapse" id="static_pages" data-parent="#sidebar-menu">
                        <div class="sub-menu">

                            <li id="static_pages_create">
                                <a class="sidenav-item-link" href="{{route('admin.static_pages.create')}}">
                                    <span class="nav-text">{{tr('add_static_page')}}</span>

                                </a>
                            </li>

                            <li id="static_pages_view">
                                <a class="sidenav-item-link" href="{{route('admin.static_pages.index')}}">
                                    <span class="nav-text">{{tr('view_static_pages')}}</span>

                                </a>
                            </li>

                        </div>
                    </ul>

                </li>

                <li class="section-title">{{tr('setting_management')}}</li>

                <li id="settings">
                    <a class="sidenav-item-link" href="{{route('admin.settings')}}">
                        <i class="mdi mdi-settings"></i>
                        <span class="nav-text">{{tr('settings')}}</span>
                    </a>
                </li>

                <li class="section-title">
                    {{tr('profile_management')}}
                </li>

                <li id="profile">
                    <a class="sidenav-item-link" href="{{route('admin.profile')}}">
                        <i class="mdi mdi-account"></i>
                        <span class="nav-text">{{tr('profile')}}</span>
                    </a>
                </li>

                <li>
                    <a class="sidenav-item-link" href="{{route('admin.logout')}}" data-toggle="modal" data-target="#logoutModel">
                        <i class="mdi mdi-logout"></i>
                        <span class="nav-text">{{tr('logout')}}</span>
                    </a>
                </li>
            </ul>

        </div>


    </div>
</aside>

<script type="text/javascript">
    // Target id == Page 
    @if(isset($main_page))
    $("#{{$main_page}}").addClass("active expand");
    $("#{{$main_page}}").find('.sidenav-item-link').click();
    @endif

    @if(isset($page))
    $("#{{$page}}").addClass("show");
    @endif

    @if(isset($sub_page))
    $("#{{$sub_page}}").addClass("active");
    @endif
</script>