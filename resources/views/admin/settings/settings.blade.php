@extends('layouts.admin') 

@section('page_header', tr('settings'))

@section('breadcrumbs')

    <li class="breadcrumb-item active" aria-current="page">
        <span>{{ tr('settings') }}</span>
    </li>

    <li class="breadcrumb-item active" aria-current="page">

    <span id="settings-breadcrumb">{{ tr('site_settings') }}</span>
</li>
           
@endsection 

@section('styles')

<style>
    .nav-item {
        width: 100%
    }
    
</style>

@endsection 

@section('content')
<div class="row">
    <div class="col-sm-3">

        <div class="card card-default card-profile">

            <div class="card-body card-profile-body">
                <h4>{{ tr('settings') }}</h4>
            </div>
        
            <div class="card-footer card-profile-footer">

                <ul class="nav nav-border-top" id="pills-tab2" role="tablist">

                    <li class="d-block mb-4 nav-item">
                        <a class="nav-link active" id="nav-profile-tab" data-toggle="pill" href="#site_settings" role="tab"
                        aria-controls="nav-profile" aria-selected="false">
                            <i class="mdi mdi-account-settings mr-2"></i> {{tr('site_settings')}}
                        </a>
                    </li>

                    <li class="d-block mb-4 nav-item">
                        <a class="nav-link" id="nav-profile-tab" data-toggle="pill" href="#payment_settings" role="tab"
                        aria-controls="nav-profile" aria-selected="false">
                            <i class="mdi mdi-account-card-details mr-2"></i> {{tr('payment_settings')}}
                        </a>
                    </li>

                    <li class="d-block mb-4 nav-item">
                      <a class="nav-link" id="nav-profile-tab" data-toggle="pill" href="#email_settings" role="tab"
                        aria-controls="nav-profile" aria-selected="false">
                        <i class="mdi mdi-playlist-edit mr-2"></i> {{tr('email_settings')}}</a>
                    </li>

                    <li class="d-block mb-4 nav-item">
                        <a class="nav-link" id="nav-profile-tab" data-toggle="pill" href="#social_settings" role="tab"
                        aria-controls="nav-profile" aria-selected="false">
                            <i class="mdi mdi-open-in-new mr-2"></i>{{tr('social_settings')}}
                        </a>
                    </li>

                    <li class="d-bloc mb-4 nav-item">
                        <a class="nav-link" id="nav-profile-tab" data-toggle="pill" href="#contact_information_settings" role="tab"
                        aria-controls="nav-profile" aria-selected="false">
                            <i class="mdi mdi-message-text mr-2"></i> {{tr('contact_information_settings')}}
                        </a>
                    </li>

                    <!-- <li class="d-bloc mb-4 nav-item">
                        <a class="nav-link" id="nav-profile-tab" data-toggle="pill" href="#notification_settings" role="tab"
                        aria-controls="nav-profile" aria-selected="false">
                            <i class="mdi mdi-message-text mr-2"></i> {{tr('notification_settings')}}
                        </a>
                    </li> -->

                    <!-- <li class="d-block mb-4 nav-item">
                        <a class="nav-link" id="nav-profile-tab" data-toggle="pill" href="#social_login" role="tab"
                        aria-controls="nav-profile" aria-selected="false">
                            <i class="mdi mdi-facebook-box mr-2"></i> {{tr('social_login')}}
                        </a>
                    </li> -->

                    <li class="d-block mb-4 nav-item">
                        <a class="nav-link" id="nav-profile-tab" data-toggle="pill" href="#other_settings" role="tab"
                        aria-controls="nav-profile" aria-selected="false">
                            <i class="mdi mdi-ticket-percent mr-2"></i> {{tr('other_settings')}}
                        </a>
                    </li>

                    <li class="d-bloc mb-4 nav-item">
                        <a class="nav-link flex" id="nav-profile-tab" data-toggle="pill" href="#bank_account_information_settings" role="tab"
                        aria-controls="nav-profile" aria-selected="false">
                            <i class="mdi mdi-bank mr-2"></i> {{tr('bank_account_information_settings')}}
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-sm-9">
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="site_settings" role="tabpanel" aria-labelledby="nav-profile-tab">

                <div class="card card-default">

                    <div class="card-header">
                        <h2 class="text-uppercase">{{tr('site_settings')}}</h2>
                    </div>

                    <div class="card-body">

                        <form id="site_settings_save" action="{{ route('admin.settings.save') }}" method="POST" enctype="multipart/form-data" role="form">

                            @csrf

                            <div class="row">

                                <div class="form-group col-md-6">
                                    <label for="site_name">{{tr('site_name')}} *</label>
                                    <input type="text" class="form-control" id="site_name" name="site_name" placeholder="Enter {{tr('site_name')}}" value="{{Setting::get('site_name')}}" required>
                                
                                </div>

                                <div class="form-group col-md-6">

                                    <label for="frontend_url">{{tr('frontend_url')}} *</label>

                                    <input type="text" class="form-control" id="frontend_url" name="frontend_url" placeholder="{{tr('frontend_url')}}" value="{{Setting::get('frontend_url')}}" required>

                                </div>

                                <div class="form-group col-md-12" style="margin-left:auto">

                                    <label for="tag_name">{{tr('tag_name')}} *</label>

                                    <input type="text" class="form-control" id="tag_name" name="tag_name" placeholder="{{tr('tag_name')}}" value="{{Setting::get('tag_name')}}" required>

                                </div>

                                <div class="form-group col-md-6">
                                    <label for="site_logo">{{tr('site_logo')}} *</label>
                                    <p class="txt-warning">{{tr('png_image_note')}}</p>
                                    <input type="file" class="form-control" id="site_logo" name="site_logo" accept="image/png" placeholder="{{tr('site_logo')}}">
                                
                                </div>

                                @if(Setting::get('site_logo'))

                                <div class="form-group col-md-6">

                                    <img class="img img-thumbnail m-b-20" style="width: 40%" src="{{Setting::get('site_logo')}}" alt="{{Setting::get('site_name')}}"> 
                                </div>

                                @endif

                                <div class="form-group col-md-6">

                                    <label for="site_icon">{{tr('site_icon')}} *</label>

                                    <p class="txt-warning">{{tr('png_image_note')}}</p>

                                    <input type="file" class="form-control" id="site_icon" name="site_icon" accept="image/png" placeholder="{{tr('site_icon')}}">

                                </div>

                                @if(Setting::get('site_icon'))

                                <div class="form-group col-md-6">

                                    <img class="img img-thumbnail m-b-20" style="width: 20%" src="{{Setting::get('site_icon')}}" alt="{{Setting::get('site_name')}}"> 

                                </div>

                                @endif

                            </div>

                            <div class="box-footer">

                                <button class="btn reset-btn btn-pill " type="reset">{{tr('reset')}}</button>

                                @if(Setting::get('admin_delete_control') == YES)
                                    <button type="button" class="btn btn-primary btn-pill mb-5 pull-right" disabled>{{tr('save')}}</button>
                                @else
                                    <button type="submit" class="btn btn-primary btn-pill mr-2 pull-right" >{{tr('save')}}</button>
                                @endif
                   
                            </div>

                        </form>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="payment_settings" role="tabpanel" aria-labelledby="nav-profile-tab">
                
                <div class="card card-default">

                    <div class="card-header">
                        <h2 class="text-uppercase">{{tr('payment_settings')}} - {{tr('stripe_settings')}}</h2>
                    </div>

                    <div class="card-body">

                        <form id="site_settings_save" action="{{route('admin.settings.save')}}" method="POST" enctype="multipart/form-data" class="forms-sample">
     
                            @csrf
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">

                                        <label for="stripe_publishable_key">{{tr('stripe_publishable_key')}} *</label>

                                        <input type="text" class="form-control" id="stripe_publishable_key" name="stripe_publishable_key" placeholder="Enter {{tr('stripe_publishable_key')}}" value="{{old('stripe_publishable_key') ?: Setting::get('stripe_publishable_key')}}">

                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="stripe_secret_key">{{tr('stripe_secret_key')}} *</label>

                                        <input type="text" class="form-control" id="stripe_secret_key" name="stripe_secret_key" placeholder="Enter {{tr('stripe_secret_key')}}" value="{{old('stripe_secret_key') ?: Setting::get('stripe_secret_key')}}">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <label for="stripe_secret_key">{{tr('stripe_mode')}} *</label>

                                    <div class="clearfix"></div>

                                    <div class="radio radio-aqua" style="display: inline-block;">

                                        <input id="stripe_live" name="stripe_mode" type="radio" value="{{ STRIPE_MODE_LIVE }}" @if(Setting::get('stripe_mode') == STRIPE_MODE_LIVE ) checked="checked" @endif>

                                        <label for="stripe_live">
                                            {{tr('live')}}
                                        </label>

                                    </div>

                                    <div class="radio radio-yellow" style="display: inline-block;">
                                        <input id="stripe_sandbox" name="stripe_mode" type="radio" value="{{ STRIPE_MODE_SANDBOX }}" @if(Setting::get( 'stripe_mode') == STRIPE_MODE_SANDBOX) checked="checked" @endif>
                                        <label for="stripe_sandbox">
                                            {{tr('sandbox')}}
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="box-footer">

                                <button class="btn btn-light btn-pill" type="reset">{{tr('reset')}}</button>

                                @if(Setting::get('admin_delete_control') == YES)
                                    <button type="button" class="btn btn-primary btn-pill mb-5 pull-right" disabled>{{tr('save')}}</button>
                                @else
                                    <button type="submit" class="btn btn-primary btn-pill mr-2 pull-right" >{{tr('save')}}</button>
                                @endif
                   
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        

            <div class="tab-pane fade" id="email_settings" role="tabpanel" aria-labelledby="nav-profile-tab">
            
                <div class="card card-default">

                    <div class="card-header">
                        <h2 class="text-uppercase">{{tr('email_settings')}}</h2>
                    </div>

                    <form id="site_settings_save" action="{{route('admin.env-settings.save')}}" method="POST">

                        @csrf

                        <div class="card-body">
                            <div class="row">

                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="MAIL_DRIVER">{{tr('MAIL_DRIVER')}} *</label>
                                        <p class="text-muted">{{tr('mail_driver_note')}}</p>
                                        <input type="text" class="form-control" id="MAIL_DRIVER" name="MAIL_DRIVER" placeholder="Enter {{tr('MAIL_DRIVER')}}" value="{{old('MAIL_DRIVER') ?: $env_values['MAIL_DRIVER'] }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="MAIL_HOST">{{tr('MAIL_HOST')}} *</label>
                                        <p class="text-muted">{{tr('mail_host_note')}}</p>

                                        <input type="text" class="form-control" id="MAIL_HOST" name="MAIL_HOST" placeholder="Enter {{tr('MAIL_HOST')}}" value="{{old('MAIL_HOST') ?: $env_values['MAIL_HOST']}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="MAIL_FROM_ADDRESS">{{tr('MAIL_FROM_ADDRESS')}} *</label>

                                        <p class="text-muted">{{tr('MAIL_FROM_ADDRESS_note')}}</p>

                                        <input type="text" class="form-control" id="MAIL_FROM_ADDRESS" name="MAIL_FROM_ADDRESS" placeholder="Enter {{tr('MAIL_FROM_ADDRESS')}}" value="{{old('MAIL_FROM_ADDRESS') ?: ($env_values['MAIL_FROM_ADDRESS'] ?? '')}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="MAIL_PORT">{{tr('MAIL_PORT')}} *</label>

                                        <p class="text-muted">{{tr('mail_port_note')}}</p>

                                        <input type="text" class="form-control" id="MAIL_PORT" name="MAIL_PORT" placeholder="Enter {{tr('MAIL_PORT')}}" value="{{old('MAIL_PORT') ?: ($env_values['MAIL_PORT'] ?? '')}}">
                                    </div>
                                </div>

                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="MAIL_USERNAME">{{tr('MAIL_USERNAME')}} *</label>

                                        <p class="text-muted">{{tr('mail_username_note')}}</p>

                                        <input type="text" class="form-control" id="MAIL_USERNAME" name="MAIL_USERNAME" placeholder="Enter {{tr('MAIL_USERNAME')}}" value="{{old('MAIL_USERNAME') ?: ($env_values['MAIL_USERNAME'] ?? '')}}">
                                    </div>

                                    <div class="form-group">

                                        <label for="MAIL_PASSWORD">{{tr('MAIL_PASSWORD')}} *</label>

                                        <p class="text-muted" style="visibility: hidden;">{{tr('mail_username_note')}}</p>

                                        <input type="password" class="form-control" id="MAIL_PASSWORD" name="MAIL_PASSWORD" placeholder="Enter {{tr('MAIL_PASSWORD')}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="MAIL_FROM_NAME">{{tr('MAIL_FROM_NAME')}} *</label>

                                        <p class="text-muted">{{tr('MAIL_FROM_NAME_note')}}</p>

                                        <input type="text" class="form-control" id="MAIL_FROM_NAME" name="MAIL_FROM_NAME" placeholder="Enter {{tr('MAIL_FROM_NAME')}}" value="{{old('MAIL_FROM_NAME') ?: ($env_values['MAIL_FROM_NAME'] ?? '')}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="MAIL_ENCRYPTION">{{tr('MAIL_ENCRYPTION')}} *</label>

                                        <p class="text-muted">{{tr('mail_encryption_note')}}</p>

                                        <input type="text" class="form-control" id="MAIL_ENCRYPTION" name="MAIL_ENCRYPTION" placeholder="Enter {{tr('MAIL_ENCRYPTION')}}" value="{{old('MAIL_ENCRYPTION') ?: $env_values['MAIL_ENCRYPTION']}}">
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                        </div>

                        <div class="card-footer">

                            <button class="btn btn-light btn-pill" type="reset">{{tr('reset')}}</button>

                            @if(Setting::get('admin_delete_control') == YES)
                                <button type="button" class="btn btn-primary btn-pill mb-5 pull-right" disabled>{{tr('save')}}</button>
                            @else
                                <button type="submit" class="btn btn-primary btn-pill mr-2 pull-right" >{{tr('save')}}</button>
                            @endif

                        </div>
                    </form>
                </div>

            </div>

            <div class="tab-pane fade" id="social_settings" role="tabpanel" aria-labelledby="nav-profile-tab">
                
                <div class="card card-default">

                    <div class="card-header">
                        <h2 class="text-uppercase">{{tr('social_settings')}}</h2>
                    </div>

                    <div class="card-body">

                        <form id="site_settings_save" action="{{route('admin.settings.save')}}" method="POST">
                    
                            @csrf

                            <div class="box-body">
                                <div class="row">

                                    <div class="col-md-12">

                                        <h5><b>{{tr('site_url_settings')}}</b></h5>

                                        <hr>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="facebook_link">{{tr('facebook_link')}} *</label>

                                            <input type="text" class="form-control" id="facebook_link" name="facebook_link" placeholder="Enter {{tr('facebook_link')}}" value="{{old('facebook_link') ?: Setting::get('facebook_link')}}">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="linkedin_link">{{tr('linkedin_link')}} *</label>

                                            <input type="text" class="form-control" id="linkedin_link" name="linkedin_link" placeholder="Enter {{tr('linkedin_link')}}" value="{{old('linkedin_link') ?: Setting::get('linkedin_link')}}">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="twitter_link">{{tr('twitter_link')}} *</label>

                                            <input type="text" class="form-control" id="twitter_link" name="twitter_link" placeholder="Enter {{tr('twitter_link')}}" value="{{old('twitter_link') ?: Setting::get('twitter_link')}}">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="pinterest_link">{{tr('pinterest_link')}} *</label>

                                            <input type="text" class="form-control" id="pinterest_link" name="pinterest_link" placeholder="Enter {{tr('pinterest_link')}}" value="{{old('pinterest_link') ?: Setting::get('pinterest_link')}}">
                                        </div>
                                    </div>
                                    
                                    <div class="clearfix"></div>
                                    
                                </div>
                            
                            </div>
                            
                            <div class="box-footer">

                                <button class="btn btn-light btn-pill" type="reset">{{tr('reset')}}</button>

                                @if(Setting::get('admin_delete_control') == YES)
                                    <button type="button" class="btn btn-primary btn-pill mb-5 pull-right" disabled>{{tr('save')}}</button>
                                @else
                                    <button type="submit" class="btn btn-primary btn-pill mr-2 pull-right" >{{tr('save')}}</button>
                                @endif

                            </div>
                    
                        </form>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="social_login" role="tabpanel" aria-labelledby="nav-profile-tab">

                <div class="card card-default">

                    <div class="card-header">
                        <h2 class="text-uppercase">{{tr('social_login')}}</h2>
                    </div>

                    <div class="card-body">
                        <form id="social_settings_save" action="{{route('admin.settings.save')}}" method="POST">
                    
                            @csrf

                            <div class="box-body">

                                <div class="row">

                                    <div class="col-md-12">
                                        <h5><b>{{tr('fb_settings')}}</b></h5>
                                        <hr>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="FB_CLIENT_ID">{{tr('FB_CLIENT_ID')}} *</label>

                                            <input type="text" class="form-control" name="FB_CLIENT_ID" id="FB_CLIENT_ID" placeholder="Enter {{tr('FB_CLIENT_ID')}}" value="{{old('FB_CLIENT_ID') ?: Setting::get('FB_CLIENT_ID') }}">
                                        </div>
                                    </div>
                                   
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="FB_CLIENT_SECRET">{{tr('FB_CLIENT_SECRET')}} *</label>

                                            <input type="text" class="form-control" name="FB_CLIENT_SECRET" id="FB_CLIENT_SECRET" placeholder="Enter {{tr('FB_CLIENT_SECRET')}}" value="{{old('FB_CLIENT_SECRET') ?: Setting::get('FB_CLIENT_SECRET') }}">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="FB_CALL_BACK">{{tr('FB_CALL_BACK')}} *</label>

                                            <input type="text" class="form-control" name="FB_CALL_BACK" id="FB_CALL_BACK" placeholder="Enter {{tr('FB_CALL_BACK')}}" value="{{old('FB_CALL_BACK') ?: Setting::get('FB_CALL_BACK') }}">
                                        </div>
                                    </div>

                                    <div class="col-md-12">

                                        <h5><b>{{tr('google_settings')}}</b></h5>

                                        <hr>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="GOOGLE_CLIENT_ID">{{tr('GOOGLE_CLIENT_ID')}} *</label>

                                            <input type="text" class="form-control" name="GOOGLE_CLIENT_ID" id="GOOGLE_CLIENT_ID" placeholder="Enter {{tr('GOOGLE_CLIENT_ID')}}" value="{{old('GOOGLE_CLIENT_ID') ?: Setting::get('GOOGLE_CLIENT_ID') }}">
                                        </div>
                                    </div>
                                   
                                     <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="GOOGLE_CLIENT_SECRET">{{tr('GOOGLE_CLIENT_SECRET')}} *</label>

                                            <input type="text" class="form-control" name="GOOGLE_CLIENT_SECRET" id="GOOGLE_CLIENT_SECRET" placeholder="Enter {{tr('GOOGLE_CLIENT_SECRET')}}" value="{{old('GOOGLE_CLIENT_SECRET') ?: Setting::get('GOOGLE_CLIENT_SECRET') }}">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="GOOGLE_CALL_BACK">{{tr('GOOGLE_CALL_BACK')}} *</label>

                                            <input type="text" class="form-control" name="GOOGLE_CALL_BACK" id="GOOGLE_CALL_BACK" placeholder="Enter {{tr('GOOGLE_CALL_BACK')}}" value="{{old('GOOGLE_CALL_BACK') ?: Setting::get('GOOGLE_CALL_BACK') }}">
                                        </div>
                                    </div>

                                </div>
                            
                            </div>
                        
                            <div class="box-footer">

                                <button class="btn btn-light btn-pill" type="reset">{{tr('reset')}}</button>

                                @if(Setting::get('admin_delete_control') == YES)
                                    <button type="button" class="btn btn-primary btn-pill mb-5 pull-right" disabled>{{tr('save')}}</button>
                                @else
                                    <button type="submit" class="btn btn-primary btn-pill mr-2 pull-right" >{{tr('save')}}</button>
                                @endif

                            </div>

                        </form>
                    </div>
                </div>

            </div>

            <div class="tab-pane fade" id="notification_settings" role="tabpanel" aria-labelledby="nav-profile-tab">
                
                <div class="card card-default">

                    <div class="card-header">
                        <h2 class="text-uppercase">{{tr('notification_settings')}}</h2>
                    </div>

                    <div class="card-body">

                        <form id="social_settings_save" action="{{route('admin.settings.save')}}" method="POST">
                    
                            @csrf
                            
                            <div class="box-body">
                            
                                <div class="row">

                                    <div class="col-md-12">
                                        <h5><b>{{tr('fcm_details')}}</b></h5>
                                        <hr>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">

                                            <label for="user_fcm_sender_id">{{ tr('user_fcm_sender_id') }}</label>

                                            <input type="text" class="form-control" name="user_fcm_sender_id" id="user_fcm_sender_id"
                                            value="{{ Setting::get('user_fcm_sender_id') }}" placeholder="{{ tr('user_fcm_sender_id') }}">
                                        </div>
                                    </div>  

                                    <div class="col-md-6">
                                        <div class="form-group">

                                            <label for="user_fcm_server_key">{{ tr('user_fcm_server_key') }}</label>

                                            <input type="text" class="form-control" name="user_fcm_server_key" id="user_fcm_server_key"
                                            value="{{ Setting::get('user_fcm_server_key') }}" placeholder="{{ tr('user_fcm_server_key') }}">
                                        </div>
                                    </div> 

                                </div>  
                    
                            </div> 

                            <div class="box-footer">

                                <button class="btn reset-btn btn-pill" type="reset">{{tr('reset')}}</button>

                                @if(Setting::get('admin_delete_control') == YES)
                                    <button type="button" class="btn btn-primary btn-pill mb-5 pull-right" disabled>{{tr('save')}}</button>
                                @else
                                    <button type="submit" class="btn btn-primary btn-pill mr-2 pull-right" >{{tr('save')}}</button>
                                @endif
                           
                            </div> 
                        
                        </form>
                    </div>
                </div>

            </div>

            <div class="tab-pane fade" id="apps_settings" role="tabpanel" aria-labelledby="nav-profile-tab">

                <div class="card card-default">

                    <div class="card-header">
                        <h2 class="text-uppercase">{{tr('apps_settings')}}</h2>
                    </div>

                    <div class="card-body">
                        <form id="site_settings_save" action="{{route('admin.settings.save')}}" method="POST">
                    
                            @csrf
                            
                            <div class="box-body">
                                    
                                <div class="row">

                                    <div class="col-md-12">

                                        <h5><b>{{tr('mobile_settings')}}</b></h5>

                                        <hr>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="playstore_user">{{tr('playstore_user')}} *</label>
                                            <input type="text" class="form-control" id="playstore_user" name="playstore_user" placeholder="Enter {{tr('playstore_user')}}" value="{{old('playstore_user') ?: Setting::get('playstore_user')}}">
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                         <div class="form-group">
                                            <label for="appstore_user">{{tr('appstore_user')}} *</label>

                                            <input type="text" class="form-control" id="appstore_user" name="appstore_user" placeholder="Enter {{tr('appstore_user')}}" value="{{old('appstore_user') ?: Setting::get('appstore_user')}}">
                                        </div>
                                    </div>
                                    
                                </div>

                            </div>

                            <div class="box-footer">

                                <button class="btn reset-btn btn-pill" type="reset">{{tr('reset')}}</button>

                                @if(Setting::get('admin_delete_control') == YES)
                                    <button type="button" class="btn btn-primary btn-pill mb-5 pull-right" disabled>{{tr('save')}}</button>
                                @else
                                    <button type="submit" class="btn btn-primary btn-pill mr-2 pull-right" >{{tr('save')}}</button>
                                @endif
                   
                            </div>
                   
                        </form>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="contact_information_settings" role="tabpanel" aria-labelledby="nav-profile-tab">

                <div class="card card-default">

                    <div class="card-header">
                        <h2 class="text-uppercase">{{tr('contact_information')}}</h2>
                    </div>

                    <div class="card-body">

                        <form id="contact_information_save" action="{{ route('admin.settings.save') }}" method="POST" enctype="multipart/form-data" role="form">

                            @csrf
                            
                            <div class="row">

                                <div class="form-group col-md-6">
                                    <label for="copyright_content">{{tr('copyright_content')}}</label>
                                    <input type="text" class="form-control" id="copyright_content" name="copyright_content" placeholder="Enter {{tr('copyright_content')}}" value="{{Setting::get('copyright_content')}}">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="contact_mobile">{{tr('contact_mobile')}}</label>
                                    <input type="text" class="form-control" id="contact_mobile" name="contact_mobile" placeholder="Enter {{tr('contact_mobile')}}" value="{{Setting::get('contact_mobile')}}">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="contact_email">{{tr('contact_email')}}</label>
                                    <input type="text" class="form-control" id="contact_email" name="contact_email" placeholder="Enter {{tr('contact_email')}}" value="{{Setting::get('contact_email')}}">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="contact_address">{{tr('contact_address')}}</label>

                                    <textarea id="ckeditor" class="form-control" name="contact_address" placeholder="Enter {{tr('contact_address')}}"></textarea>

                                </div>

                            </div>

                            <div class="box-footer">

                                <button class="btn reset-btn btn-pill " type="reset">{{tr('reset')}}</button>

                                @if(Setting::get('admin_delete_control') == YES)
                                    <button type="button" class="btn btn-primary btn-pill mb-5 pull-right" disabled>{{tr('save')}}</button>
                                @else
                                    <button type="submit" class="btn btn-primary btn-pill mr-2 pull-right" >{{tr('save')}}</button>
                                @endif
                   
                            </div>

                        </form>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="other_settings" role="tabpanel" aria-labelledby="nav-profile-tab">
                
                <div class="card card-default">

                    <div class="card-header">
                       <h2 class="text-uppercase">{{tr('other_settings')}}</h2>
                    </div>

                    <div class="card-body">

                        <form id="site_settings_save" action="{{route('admin.settings.save')}}" method="POST">
                    
                            @csrf
                            
                            <div class="box-body"> 
                                <div class="row"> 

                                    <div class="col-md-12">

                                        <!-- <h5><b>{{tr('other_settings')}}</b></h5> -->

                                        <hr>

                                    </div>

                                    <div class="col-lg-12">

                                        <div class="form-group">
                                            <label for="google_analytics">{{tr('google_analytics')}}</label>
                                            <textarea class="form-control" id="google_analytics" name="google_analytics">{{Setting::get('google_analytics')}}</textarea>
                                        </div>

                                    </div> 

                                    <div class="col-lg-12">

                                        <div class="form-group">
                                            <label for="header_scripts">{{tr('header_scripts')}}</label>
                                            <textarea class="form-control" id="header_scripts" name="header_scripts">{{Setting::get('header_scripts')}}</textarea>
                                        </div>

                                    </div> 

                                    <div class="col-lg-12">

                                        <div class="form-group">
                                            <label for="body_scripts">{{tr('body_scripts')}}</label>
                                            <textarea class="form-control" id="body_scripts" name="body_scripts">{{Setting::get('body_scripts')}}</textarea>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">

                                <button class="btn reset-btn btn-pill" type="reset">{{tr('reset')}}</button>

                                @if(Setting::get('admin_delete_control') == YES)
                                    <button type="button" class="btn btn-primary btn-pill mb-5 pull-right" disabled>{{tr('save')}}</button>
                                @else
                                    <button type="submit" class="btn btn-primary btn-pill mr-2 pull-right" >{{tr('save')}}</button>
                                @endif
                   
                            </div>

                        </form>
                    </div>
                </div>

            </div>

            <div class="tab-pane fade" id="bank_account_information_settings" role="tabpanel" aria-labelledby="nav-profile-tab">
            <div class="card card-default">

                    <div class="card-header">
                        <h2 class="text-uppercase">{{tr('bank_account_information_settings')}}</h2>
                    </div>

                    <div class="card-body">

                        <form id="bank_account_information_settings" action="{{ route('admin.settings.save') }}" method="POST" enctype="multipart/form-data" role="form">

                            @csrf

                            <div class="row">

                                <div class="form-group col-md-6">
                                    <label for="account_number">{{tr('account_number')}} *</label>
                                    <input type="text" class="form-control" id="account_number" name="account_number" placeholder="Enter {{tr('account_number')}}" value="{{Setting::get('account_number')}}" required>
                                
                                </div>

                                <div class="form-group col-md-6">

                                    <label for="account_holder_name">{{tr('account_holder_name')}} *</label>

                                    <input type="text" class="form-control" id="account_holder_name" name="account_holder_name" placeholder="Enter {{tr('account_holder_name')}}" value="{{Setting::get('account_holder_name')}}" required>

                                </div>

                                <div class="form-group col-md-6">

                                    <label for="tag_name">{{tr('ifsc_code')}} *</label>

                                    <input type="text" class="form-control" id="ifsc_code" name="ifsc_code" placeholder="Enter {{tr('ifsc_code')}}" value="{{Setting::get('ifsc_code')}}" required>

                                </div>

                                <div class="form-group col-md-6">
                                    <label for="swift_code">{{tr('swift_code')}} *</label>
                                    <input type="text" class="form-control" id="swift_code" name="swift_code" placeholder="Enter {{tr('swift_code')}}" value="{{Setting::get('swift_code')}}" required>
                                
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="swift_code">{{tr('branch_name')}} *</label>
                                    <input type="text" class="form-control" id="branch_name" name="branch_name" placeholder="Enter {{tr('branch_name')}}" value="{{Setting::get('branch_name')}}" required>
                                
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="swift_code">{{tr('bank_name')}} *</label>
                                    <input type="text" class="form-control" id="bank_name" name="bank_name" placeholder="Enter {{tr('bank_name')}}" value="{{Setting::get('bank_name')}}" required>
                                
                                </div>


                            </div>

                            <div class="box-footer">

                                <button class="btn reset-btn btn-pill " type="reset">{{tr('reset')}}</button>

                                @if(Setting::get('admin_delete_control') == YES)
                                    <button type="button" class="btn btn-primary btn-pill mb-5 pull-right" disabled>{{tr('save')}}</button>
                                @else
                                    <button type="submit" class="btn btn-primary btn-pill mr-2 pull-right" >{{tr('save')}}</button>
                                @endif
                   
                            </div>

                        </form>
                    </div>
                </div>

           </div>


        </div>
    </div>
</div>


@endsection


@section('scripts')

<script type="text/javascript">
    
    $(document).on('click','.nav-link',function(){

        var current_setting = $(this).html();

        $('#settings-breadcrumb').html(current_setting);
    });
</script>
@endsection