@extends('layouts.admin')

@section('page_header', tr('static_pages'))

@section('breadcrumbs')

    <li class="breadcrumb-item">
    	<a href="{{ route('admin.static_pages.index') }}">{{tr('static_pages')}}</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">
    	<span>{{tr('add_static_page')}}</span>
    </li>
           
@endsection 

@section('content')
    
<div class="card">

    <div class="card-header bg-info">

        <h4 class="m-b-0 text-white">{{tr('add_static_page')}}

            <a class="btn btn-white pull-right" href="{{route('admin.static_pages.index')}}">

                <i class="fa fa-plus"></i> {{tr('view_static_pages')}}
            </a>
        </h4>

    </div>

    @include('admin.static-pages._form')

</div>

@endsection
