<div class="card-body">

    @if(Setting::get('is_demo_control_enabled') == NO )

        <form id="pages-form" class="forms-sample" action="{{ route('admin.static_pages.save') }}" method="POST" enctype="multipart/form-data" role="form">

    @else

        <form id="pages-form" class="forms-sample" role="form" enctype="multipart/form-data" role="form">

    @endif 

        @csrf

        <div class="form-body">

            <h3 class="card-title">{{tr('static_page_info')}}</h3>

            <hr>

            @if($static_page_details->id)

                <input type="hidden" name="static_page_id" value="{{$static_page_details->id}}">

            @endif

            <div class="row">

                <div class="form-group col-md-6">
                    
                    <label for="title">{{tr('title')}}<span class="admin-required">*</span> </label>
                    <input type="text" id="title" name="title" class="form-control" placeholder="Enter {{tr('title')}}" value="{{old('title')?: $static_page_details->title}}" required>
                    
                </div>

                <div class="form-group col-md-6">

                    <label for="page">
                        {{tr('select_static_page_type')}}
                        <span class="required" aria-required="true"> <span class="admin-required">*</span> </span>
                    </label>
                    
                    <select class="form-control select2" name="type" required>
                        <option value="">{{tr('select_static_page_type')}}</option>

                        @foreach($static_keys as $value)

                            <option value="{{$value}}" @if($value == $static_page_details->type) selected="true" @endif>{{ $value }}</option>

                        @endforeach 
                    </select>
                    
                </div>

                <div class="form-group col-md-12">

                    <label for="title">{{tr('description')}}<span class="admin-required">*</span></label>
                    
                    
                    <textarea style="display: none" id="description" name="description"></textarea>

                    <div id="code-editor" contenteditable="true">{!! old('description') ?:$static_page_details->description !!}</div>
                </div>

        </div> 


        <div class="form-actions top_form_action">

            <a  class="btn reset-btn btn-pill" href="{{route('admin.static_pages.index')}}">{{tr('reset')}}</a>

            @if(Setting::get('is_demo_control_enabled') == NO )

                <button type="submit" class="btn btn-primary btn-pill mr-2 pull-right">  <i class="fa fa-check"></i>{{ tr('save') }} </button>

            @else

                <button type="button" class="btn btn-primary btn-pill mr-2 pull-right" disabled> <i class="fa fa-check"></i>{{ tr('save') }}</button>
                
            @endif

        </div>

    </form>

</div>

<script>

var quill = new Quill('#code-editor', {
    theme: 'snow',
    modules: { toolbar: true },
});

$("#pages-form").submit(function() {

    var myEditor = document.querySelector('#code-editor');

    var html = myEditor.children[0].innerHTML;

    $("#description").val(html);

});

</script>