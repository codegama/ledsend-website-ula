<div class="col-md-12 mb-2">


    <form class="col-sm-12 col-sm-offset-6 search_box_css" action="{{route('admin.resolution_center_index')}}" method="GET" role="search">


        {{csrf_field()}}

        <div class="row input-group">
            <div class="col-md-6"></div>
            <div class="col-md-3">
                <input type="text" class="form-control" name="search_key" placeholder="{{tr('resolution_center_search_placeholder')}}" value="{{Request::get('search_key')??''}}"> <span class="input-group-btn">
            </div>

            @if(Request::get('type')!='')
             <input type="hidden" name="type" value="{{Request::get('type')}}">

            @endif


            <div class="col-md-3 flex">

                <button type="submit" class="btn btn-info m-r-1">
                    <span class="glyphicon glyphicon-search"> {{tr('search')}}</span>
                </button>
                @if(Request::get('type')!='')
                <a class="btn btn-danger" href="{{route('admin.resolution_center_index',['type'=>Request::get('type')])}}">{{tr('clear')}}</a>
                @else
                <a class="btn btn-danger" href="{{route('admin.resolution_center_index')}}">{{tr('clear')}}</a>
                @endif
            </div>
        </div>

    </form>
</div>