@extends('layouts.admin')

@section('title', tr('dashboard'))

@section('content-header', tr('wallets_dashboard'))

@section('breadcrumbs')

<li class="breadcrumb-item"><a href="{{route('admin.users.index')}}">{{tr('user_wallets')}}</a></li>

<li class="breadcrumb-item active"><a href="javascript:void(0)"></a>{{tr('view_wallets')}}</li>

@endsection

@section('content')

<div class="col-lg-12">

    <div class="card card-default">
        <div class="card-header bg-info">
            <h2 class="m-b-0 text-white"> 
                {{tr('transactions')}} 
                @if($user_wallet_details)
                - 
                <a class="m-b-0 text-white" href="{{route('admin.users.view',['user_id'=> $user_wallet_details->user_id])}}">{{ $user_wallet_details->user->name ?? '-'}}</a>
                @endif
            </h2>
        </div>

        <div class="card-body">

            <div class="row">
                <!-- Frist box -->
                <div class="col-xl-4 col-md-6">
                    <div class="card card-default">
                        <div class="d-flex p-5">
                            <div class="icon-md outline-secondary rounded-circle mr-3">
                                <i class="mdi mdi-wallet-outline text-secondary"></i>
                            </div>
                            <div class="text-left">
                                <span class="h2 d-block">{{formatted_amount($user_wallet_details->total ?? 0.00)}}</span>
                                <p>{{tr('total')}}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Second box -->
                <div class="col-xl-4 col-md-6">
                    <div class="card card-default">
                        <div class="d-flex p-5">
                            <div class="icon-md outline-success rounded-circle mr-3">
                                <i class="mdi mdi-hand text-success"></i>
                            </div>
                            <div class="text-left">
                                <span class="h2 d-block">{{formatted_amount($user_wallet_details->remaining ?? 0.00)}}</span>
                                <p>{{tr('remaining')}}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Third box -->
                <div class="col-xl-4 col-md-6">
                    <div class="card card-default">
                        <div class="d-flex p-5">
                            <div class="icon-md outline-warning rounded-circle mr-3">
                                <i class="mdi mdi-water text-warning"></i>
                            </div>
                            <div class="text-left">
                                <span class="h2 d-block">{{formatted_amount($user_wallet_details->used ?? 0.00)}}</span>
                                <p>{{tr('used')}}</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            @if(!$user_wallet_payments->isEmpty())

            <div class="table-responsive">

                <table id="dataTable" class="table data-table">

                    <thead>

                        <tr>
                            <th>{{ tr('s_no') }}</th>
                            <th>{{ tr('unique_id') }}</th>
                            <th>{{ tr('payment_id') }}</th>
                            <th>{{ tr('payment_mode') }}</th>
                            <th>{{ tr('total_amount') }}</th>
                            <th>{{ tr('paid_date') }}</th>
                            <th>{{ tr('status') }}</th>
                        </tr>

                    </thead>

                    <tbody>

                        @foreach($user_wallet_payments as $i => $user_wallet_payment)

                        <tr>
                            <td>{{$i+$user_wallet_payments->firstItem()}}</td>

                            <td>{{ $user_wallet_payment->user_wallet_payment_unique_id }}</td>

                            <td>{{ $user_wallet_payment->payment_id }}</td>

                            <td>
                                {{ $user_wallet_payment->payment_mode }}
                            </td>

                            <td>
                                @if($user_wallet_payment->amount_type == WALLET_AMOUNT_TYPE_ADD)
                                    <div class="text-success">{{$user_wallet_payment->paid_amount_formatted}}</div>

                                @else

                                    <div class="text-danger">
                                        {{$user_wallet_payment->paid_amount_formatted}}
                                    </div>

                                @endif

                            </td>

                            <td>
                                {{ common_date($user_wallet_payment->paid_date,Auth::guard('admin')->user()->timezone,'d M Y') }}
                            </td>


                            <td>
                                @if($user_wallet_payment->is_admin_approved == APPROVED)

                                    <span class="badge badge-info btn_size_css verified_width">{{tr('verified')}}</span>

                                @else

                                    @if($user_wallet_payment->payment_mode == BANK_TRANSFER)

                                        <a class="btn btn-info btn-sm btn_size_css verify_btn_css" href="{{ route('admin.user_wallet_payments.status', ['user_wallet_payment_id' => $user_wallet_payment->id]) }}" onclick="return confirm(&quot;{{tr('user_wallet_verify_confirmation')}}&quot;);">
                                            {{ tr('verify') }}
                                        </a>
                                    @else

                                        <span class="badge badge-success btn_size_css verified_width">{{$user_wallet_payment->status_formatted}}</span>

                                    @endif
                                @endif

                            </td>

                        </tr>

                        @endforeach

                    </tbody>

                </table>

                <div class="pull-right">{{ $user_wallet_payments->appends(request()->input())->links()}}</div>

            </div>

            @endif
        </div>

    </div>
</div>


@endsection