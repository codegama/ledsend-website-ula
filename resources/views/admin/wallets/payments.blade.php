@extends('layouts.admin')

@section('title', tr('wallets'))

@section('page_header', tr('wallets'))

@section('breadcrumbs')

<li class="breadcrumb-item"><a href="{{route('admin.users.index')}}">{{tr('wallets')}}</a></li>

<li class="breadcrumb-item active"><a href="javascript:void(0)"></a>{{ Request::get('type')?tr('bank_transactions'):tr('transactions')}}</li>

@endsection

@section('content')

<div class="card">

    <div class="card-header bg-info">
        <h4 class="m-b-0 text-white">{{ Request::get('type')? tr('bank_transactions'):tr('transactions')}}

            @if(Request::get('type'))
            <a class="btn btn-white pull-right" href="{{route('admin.user_wallet_payments.index')}}">
                <i class="fa fa-plus"></i> {{tr('view_transactions')}}
            </a>
            @else
            <a class="btn btn-white pull-right" href="{{route('admin.user_wallet_payments.index',['type'=>BANK_TRANSFER])}}">
                <i class="fa fa-plus"></i> {{tr('view_bank_transactions')}}
            </a>

            @endif
            <button type="button" class="badge badge-square badge-outline-light" data-toggle="popover" data-content="{{ Request::get('type')? tr('bank_transactions_note'):tr('transactions_note')}}">?</button>

        </h4>

    </div>

    <div class="card-body">

        @include('admin.wallets._payments_search')

        <div class="table-responsive">

            <table id="dataTable" class="table data-table">

                <thead>
                    <tr>
                        <th>{{tr('s_no')}}</th>
                        <th>{{tr('user')}}</th>
                        <th>{{tr('payment_id')}}</th>
                        <th>{{tr('payment_mode')}}</th>
                        <th>{{tr('wallet_amount_type')}}</th>
                        <th>{{tr('paid_amount')}}</th>
                        <th>{{tr('status')}}</th>
                        <th>{{tr('action')}}</th>
                        @if(Request::get('type'))
                        @endif
                    </tr>

                </thead>

                <tbody>

                    @foreach($user_wallet_payments as $i => $wallet_payment_details)

                    <tr>
                        <td>{{$i+$user_wallet_payments->firstItem()}}</td>

                        <td>
                            <a href="{{ route('admin.users.view', ['user_id' => $wallet_payment_details->user_id]) }}">
                                {{$wallet_payment_details->user->name ?? "-"}}
                            </a>

                        </td>

                        <td>{{$wallet_payment_details->payment_id ?:tr('not-available')}}
                            <span>
                                <h6>{{common_date($wallet_payment_details->paid_date, Auth::guard('admin')->user()->timezone,'d M Y')}}</h6>
                            </span>
                        </td>

                        <td>{{$wallet_payment_details->payment_mode ?:tr('not-available')}}</td>

                        <td>
                            <span class="badge badge-info text-uppercase pull-left">{{$wallet_payment_details->payment_type}}</span>
                        </td>

                        <td>{{$wallet_payment_details->paid_amount_formatted}}</td>

                        <td>
                            @if($wallet_payment_details->status == PAID)
                            <span class="badge badge-success">{{tr('paid')}}</span>
                            @else
                            <span class="badge badge-danger">{{tr('unpaid')}}</span>
                            @endif
                        </td>

                        <td>

                            <a class="btn btn-outline-info btn-sm m-b-5" href="{{ route('admin.user_wallet_payments.view', ['user_wallet_payment_id' => $wallet_payment_details->user_wallet_payment_id]) }}" class="btn btn-sm btn-theme-outline">
                                {{tr('view')}}
                            </a>

                            @if(Request::get('type'))

                                @if($wallet_payment_details->is_admin_approved == NO)

                                    <a class="btn btn-outline-success btn-sm m-b-5" onclick="return confirm(&quot;{{tr('transaction_approve_confirmation' , $wallet_payment_details->user->name ?? '')}}&quot;);" href="{{ route('admin.user_wallet_payments.status', ['user_wallet_payment_id' => $wallet_payment_details->user_wallet_payment_id]) }}">
                                        {{tr('approve')}}
                                    </a>

                                    <a class="btn btn-outline-danger btn-sm m-b-5" onclick="return confirm(&quot;{{tr('transaction_decline_confirmation' , $wallet_payment_details->user->name ?? '')}}&quot;);" href="{{ route('admin.user_wallet_payments.status', ['user_wallet_payment_id' => $wallet_payment_details->user_wallet_payment_id]) }}">
                                        {{tr('decline')}}
                                    </a>                                

                                @endif

                            @endif

                        </td>


                    </tr>

                    @endforeach

                </tbody>

            </table>

            <div class="pull-right">{{ $user_wallet_payments->appends(request()->input())->links() }}</div>

        </div>

    </div>

</div>

@endsection