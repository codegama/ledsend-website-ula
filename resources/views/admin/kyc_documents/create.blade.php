@extends('layouts.admin') 

@section('title', tr('add_document'))

@section('breadcrumbs')

    <li class="breadcrumb-item">
    	<a href="{{ route('admin.kyc_documents.index') }}">{{tr('documents')}}</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">
    	<span>{{tr('add_document')}}</span>
    </li>
           
@endsection 

@section('content')

<div class="card">

    <div class="card-header bg-info">

        <h4 class="m-b-0 text-white">{{tr('add_document')}}

            <a class="btn btn-white pull-right" href="{{route('admin.kyc_documents.index')}}">
                <i class="fa fa-eye"></i> {{tr('view_documents')}}
            </a>
        </h4>

    </div>

    @include('admin.kyc_documents._form')

</div>

@endsection