<div class="dropdown float-right bulk-action-btn">
    <button class="badge-outline-light btn btn-white pull-right dropdown-toggle text-css" href="#" id="dropdownMenuOutlineButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-plus"></i> {{tr('bulk_action')}}
    </button>

    <div class="dropdown-menu float-right" aria-labelledby="dropdownMenuOutlineButton2">

        <a class="dropdown-item action_list" href="#" id="bulk_delete">
            {{tr('delete')}}
        </a>

        <a class="dropdown-item action_list" href="#" id="bulk_approve">
            {{ tr('approve') }}
        </a>

        <a class="dropdown-item action_list" href="#" id="bulk_decline">
            {{ tr('decline') }}
        </a>
    </div>
</div>

<div class="bulk_action">

    <form action="{{route('admin.users.bulk_action')}}" id="users_form" method="POST" role="search">

        @csrf

        <input type="hidden" name="action_name" id="action" value="">

        <input type="hidden" name="selected_users" id="selected_ids" value="">

        <input type="hidden" name="page_id" id="page_id" value="{{ (request()->page) ? request()->page : '1' }}">

    </form>

</div>