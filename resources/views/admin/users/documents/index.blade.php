@extends('layouts.admin')

@section('page_header',tr('documents'))

@section('breadcrumbs')

<li class="breadcrumb-item"><a href="{{route('admin.unverified_users.index')}}">{{tr('unverified_user')}}</a></li>

<li class="breadcrumb-item active"><a href="javascript:void(0)"></a>{{tr('documents')}}</li>

@endsection

@section('content')

<div class="card">

    <div class="card-header bg-info">

        <h4 class="m-b-0 text-white">
            <a class="m-b-0 text-white" href="{{route('admin.users.view',['user_id'=> $user_details->id])}}">{{ $user_details->name??''}}</a> - {{ tr('documents')}}

        </h4>

    </div>


    <div class="col-md-12 pull-right top_btn_css">
    @if($user_details->is_kyc_document_approved != USER_KYC_DOCUMENT_APPROVED && $user_kyc_documents->count() > 0)

        <a class="btn btn-outline-info pull-right" href="{{route('admin.users.document_verify_status',['user_id'=>$user_details->id])}}" onclick="return confirm(&quot;{{tr('user_document_verify_confirmation')}}&quot;);">
            {{tr('verify')}}
        </a>
    </div>

    @endif
    <div class="card-body">
        <div class="table-responsive">

            <table id="dataTable" class="table data-table">

                <thead>
                    <tr>
                        <th>{{ tr('s_no') }}</th>
                        <th>{{ tr('documents') }}</th>
                        <th>{{ tr('updated_on') }}</th>
                        <th>{{ tr('uploaded_by') }}</th>
                        <th>{{ tr('file') }}</th>
                    </tr>
                </thead>

                <tbody>

                    @foreach($user_kyc_documents as $index => $document_details)

                    <tr>

                        <td>{{ $index+$user_kyc_documents->firstItem() }}</td>

                        <td>
                            <a href="{{ route('admin.kyc_documents.view',['kyc_document_id' => $document_details->kyc_document_id ]) }}">{{ $document_details->documentDetails->name ?? "-"}} </a>
                        </td>

                        <td>
                            {{ common_date($document_details->updated_at, Auth::guard('admin')->user()->timezone) }}
                        </td>

                        <td>
                            {{$document_details->uploaded_by ?: tr('not_available')}}
                        </td>

                        <td>
                            <a href='{{ $document_details->document_file ? $document_details->document_file :"#"}}' target="_blank">
                                <span class="btn btn-outline-warning btn-large">{{ tr('view') }}</span>
                            </a>
                        </td>

                    </tr>

                    @endforeach
                </tbody>

            </table>

            <div class="pull-right"> {{ $user_kyc_documents->appends(request()->input())->links() }} </div>
        </div>
    </div>

</div>

@endsection