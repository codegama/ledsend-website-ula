@extends('layouts.admin')

@section('page_header',tr('dashboard'))

@section('breadcrumbs')

<li class="breadcrumb-item active"><a href="javascript:void(0)" aria-current="page"></a><span>{{tr('dashboard')}}</span></li>

@endsection

@section('content')


<div class="row">

  <div class="col-xl-3 col-md-6">
    <div class="card card-default bg-secondary">
      <div class="d-flex p-5 justify-content-between">
        <div class="icon-md bg-white rounded-circle mr-3">
          <i class="mdi mdi-account-plus-outline text-secondary"></i>
        </div>
        <div class="text-right">
          <span class="h2 d-block text-white">{{$data->total_users}}</span>
          <a href="{{route('admin.users.index')}}">
            <p class="text-white">{{tr('total_users')}}</p>
          </a>
        </div>
      </div>
    </div>
  </div>

  <div class="col-xl-3 col-md-6">
    <div class="card card-default bg-success">
      <div class="d-flex p-5 justify-content-between">
        <div class="icon-md bg-white rounded-circle mr-3">
          <i class="mdi mdi-table-edit text-success"></i>
        </div>
        <div class="text-right">
          <span class="h2 d-block text-white">{{formatted_amount_string($data->total_wallet ?? '0.00')}}</span>
          <a href="{{route('admin.user_wallets.index')}}">
            <p class="text-white">{{tr('total_wallet')}}</p>
          </a>
        </div>
      </div>
    </div>
  </div>

  <div class="col-xl-3 col-md-6">
    <div class="card card-default bg-primary">
      <div class="d-flex p-5 justify-content-between">
        <div class="icon-md bg-white rounded-circle mr-3">
          <i class="mdi mdi-content-save-edit-outline text-primary"></i>
        </div>
        <div class="text-right">
          <span class="h2 d-block text-white">{{formatted_amount_string($data->today_wallet ?? '0.00')}}</span>
          <a href="{{route('admin.user_wallets.index',['today_wallet'=>YES])}}">
            <p class="text-white">{{tr('today_wallet')}}</p>
          </a>
        </div>
      </div>
    </div>
  </div>

  <div class="col-xl-3 col-md-6">
    <div class="card card-default bg-danger">
      <div class="d-flex p-5 justify-content-between">
        <div class="icon-md bg-white rounded-circle mr-3">
          <i class="mdi mdi-bell text-info"></i>
        </div>
        <div class="text-right">
          <span class="h2 d-block text-white">{{$data->total_withdraw_requests ?? 0}}</span>
          <a href="{{route('admin.user_withdrawals.index')}}">
            <p class="text-white">{{tr('total_withdraw_requests')}}</p>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
</div>







<div class="col-lg-12">
  <div class="card card-default">
    <div class="card-header">
      <h2>{{tr('amount_added_to_wallet')}} Vs {{tr('amount_sent_to_others')}}</h2>
    </div>
    <div id="mixed-chart-1"></div>
  </div>
</div>

<div class="col-lg-6">
  <div class="card card-default">
    <div class="card-header">
      <h2>{{tr('recent_page_view_count')}}</h2>
    </div>
    <div id="aria-chart"></div>
  </div>

</div>

<div class="col-lg-6">
  <div class="card card-default">
    <div class="card-header">
      <h2>{{tr('recent_users')}}</h2>
    </div>
    <div class="card-body py-0" data-simplebar style="height: 392px;">
      <table class="table table-borderless table-thead-border">
        <thead>
          <tr>
            <th>{{tr('name')}}</th>
            <th class="text-right">{{tr('time')}}</th>
          </tr>
        </thead>
        <tbody>
          @foreach($data->recent_users as $user_details)
          <tr>
            <td>
              <a href="{{ route('admin.users.view', ['user_id' => $user_details->id]) }}">
                <img src="{{$user_details->picture ?? asset('placeholder.jpg')}}" alt="user" width="40" class="img-circle img_space" />{{$user_details->name}}
              </a>
            </td>

            <td class="text-right">{{$user_details->created_at->diffForHumans()}}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <div class="card-footer bg-white d-flex py-4">
      <a href="{{route('admin.users.index')}}" class="btn btn-primary">{{tr('view_all')}}</a>
    </div>

  </div>


</div>






</div>


@endsection


@section('scripts')

<script type="text/javascript">
  var ariaChartExample = document.querySelector("#aria-chart");
  if (ariaChartExample !== null) {
    var options = {
      chart: {
        height: 350,
        type: 'area',
        toolbar: {
          show: false,
        }
      },
      colors: ['#9e6de0', '#faafca'],
      fill: {
        colors: ['#9e6de0', '#faafca'],
        type: 'solid',
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'smooth'
      },
      series: [{
          name: 'views',
          data: <?php print_r(json_encode($views)); ?>,
        }

      ],
      legend: {
        show: false,
      },
      xaxis: {
        type: 'date',
      },
      tooltip: {
        theme: 'dark',
        x: {
          format: 'DD/MM'
        },
        y: {
          formatter: function(value, {
            series,
            seriesIndex,
            dataPointIndex,
            w
          }) {
            return value
          }
        },
      }
    }

    var chart = new ApexCharts(
      ariaChartExample,
      options
    );

    chart.render();
  }




  var mixedChart1 = document.querySelector("#mixed-chart-1");
  if (mixedChart1 !== null) {
    var mixedOptions1 = {
      chart: {
        height: 370,
        type: 'bar',
        toolbar: {
          show: false,
        }
      },
      colors: ['#9e6de0', '#faafca', '#f2e052'],
      legend: {
        show: true,
        position: 'top',
        horizontalAlign: 'right',
        markers: {
          width: 20,
          height: 5,
          radius: 0,
        },
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: '50%',
          barHeight: '10%',
          distributed: false,
        }
      },
      dataLabels: {
        enabled: false
      },

      stroke: {
        show: true,
        width: 2,
        curve: 'smooth'
      },

      series: [{
          name: "{{tr('amount_added_to_wallet')}}",
          type: 'column',
          data: [<?php foreach ($wallet_details as $wallet_add) {
                    echo '"' . $wallet_add['amount_add_to_wallet'] . '",';
                  } ?>]
        }, {
          name: "{{tr('amount_sent_to_others')}}",
          type: 'column',
          data: [<?php foreach ($wallet_details as $wallet_sent) {
                    echo '"' . $wallet_sent['sent_to_others'] . '",';
                  } ?>]
        },
        {
          name: "{{tr('profit')}}",
          data: [<?php foreach ($wallet_details as $wallet_profit) {
                    echo '"' . $wallet_profit['profit'] . '",';
                  } ?>],
          type: 'line'
        }
      ],

      xaxis: {
        categories: [<?php foreach ($wallet_details as $month) {
                        echo '"' . $month['month'] . '",';
                      } ?>],

        axisBorder: {
          show: false,
        },
        axisTicks: {
          show: false
        },
        crosshairs: {
          width: 40,
        }
      },

      fill: {
        opacity: 1

      },

      tooltip: {
        shared: true,
        intersect: false,
        followCursor: true,
        fixed: {
          enabled: false
        },
        x: {
          show: false
        },
        y: {
          title: {
            formatter: function(seriesName) {
              return seriesName
            }
          },
        },
      }

    }

    var randerMixedChart1 = new ApexCharts(mixedChart1, mixedOptions1);
    randerMixedChart1.render();
  }
</script>

@endsection